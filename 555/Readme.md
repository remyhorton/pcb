SMD 555 oscillator
==================
A 555 chip setup as an astable multi-vibrator complete with diode to provide for a 50% duty cycle.
The pads for the 555 itself and the diode are through-hole whereas those for the resistors and capacitor are 0805 surface-mount.

![Schematic](schematic.png)

## Licencing
See the [top-level Readme.md](../Readme.md) for licencing details.


## Contact

`remy.horton` `(at)` `gmail.com`