EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Myraa 74095 breakout"
Date "18 May 2023"
Rev "Rev.1"
Comp "Remy Horton"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x05_Female J8
U 1 1 64673906
P 5850 5850
F 0 "J8" V 5742 6098 50  0000 L CNN
F 1 "Conn_01x05_Female" V 5787 6098 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 5850 5850 50  0001 C CNN
F 3 "~" H 5850 5850 50  0001 C CNN
	1    5850 5850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x05_Female J5
U 1 1 64674258
P 5850 4750
F 0 "J5" V 5742 4462 50  0000 R CNN
F 1 "Conn_01x05_Female" V 5787 4998 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 5850 4750 50  0001 C CNN
F 3 "~" H 5850 4750 50  0001 C CNN
	1    5850 4750
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J2
U 1 1 64675587
P 5650 5150
F 0 "J2" V 5700 5100 50  0000 L CNN
F 1 "Conn_01x01_Female" V 5587 5198 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 5650 5150 50  0001 C CNN
F 3 "~" H 5650 5150 50  0001 C CNN
	1    5650 5150
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J1
U 1 1 64675C57
P 5550 5350
F 0 "J1" V 5600 5300 50  0000 L CNN
F 1 "Conn_01x01_Female" V 5487 5398 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 5550 5350 50  0001 C CNN
F 3 "~" H 5550 5350 50  0001 C CNN
	1    5550 5350
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J4
U 1 1 64676423
P 5750 5150
F 0 "J4" V 5800 5100 50  0000 L CNN
F 1 "Conn_01x01_Female" V 5687 5198 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 5750 5150 50  0001 C CNN
F 3 "~" H 5750 5150 50  0001 C CNN
	1    5750 5150
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J6
U 1 1 646766B7
P 5850 5150
F 0 "J6" V 5900 5100 50  0000 L CNN
F 1 "Conn_01x01_Female" V 5787 5198 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 5850 5150 50  0001 C CNN
F 3 "~" H 5850 5150 50  0001 C CNN
	1    5850 5150
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J9
U 1 1 64676E1F
P 5950 5150
F 0 "J9" V 6000 5100 50  0000 L CNN
F 1 "Conn_01x01_Female" V 5887 5198 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 5950 5150 50  0001 C CNN
F 3 "~" H 5950 5150 50  0001 C CNN
	1    5950 5150
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J3
U 1 1 64677194
P 5650 5350
F 0 "J3" V 5700 5300 50  0000 L CNN
F 1 "Conn_01x01_Female" V 5587 5398 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 5650 5350 50  0001 C CNN
F 3 "~" H 5650 5350 50  0001 C CNN
	1    5650 5350
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J7
U 1 1 646773CF
P 5850 5350
F 0 "J7" V 5900 5300 50  0000 L CNN
F 1 "Conn_01x01_Female" V 5787 5398 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 5850 5350 50  0001 C CNN
F 3 "~" H 5850 5350 50  0001 C CNN
	1    5850 5350
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J10
U 1 1 646776A2
P 5950 5350
F 0 "J10" V 6000 5300 50  0000 L CNN
F 1 "Conn_01x01_Female" V 5887 5398 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 5950 5350 50  0001 C CNN
F 3 "~" H 5950 5350 50  0001 C CNN
	1    5950 5350
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J11
U 1 1 64677945
P 6050 5350
F 0 "J11" V 6100 5300 50  0000 L CNN
F 1 "Conn_01x01_Female" V 5987 5398 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 6050 5350 50  0001 C CNN
F 3 "~" H 6050 5350 50  0001 C CNN
	1    6050 5350
	0    -1   -1   0   
$EndComp
NoConn ~ 6050 4950
Wire Wire Line
	5650 5650 5550 5650
Wire Wire Line
	5550 5650 5550 5550
Wire Wire Line
	5750 5550 5750 5650
Wire Wire Line
	5650 5550 5750 5550
Wire Wire Line
	5850 5550 5850 5650
Wire Wire Line
	5950 5550 5950 5650
Wire Wire Line
	6050 5550 6050 5650
$EndSCHEMATC
