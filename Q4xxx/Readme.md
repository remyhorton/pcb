Breakout for Coilcraft flyback transformer
==========================================
This PCB is a mini breakout board that allows through-hole Coilcraft flyback transformers to be mounted on standard 2.54mm breadboard, as these components have an offset polarity pin that prevents them being mounted directly.
In the picture below there is a S6000-AL in place but the footprint used on the board can also take the Q4434-BL, Q4337-BL, and Q4339-BL.

![Circuit](circuit.jpeg)

## Licencing
See the [top-level Readme.md](../Readme.md) for licencing details.


## Contact

`remy.horton` `(at)` `gmail.com`
