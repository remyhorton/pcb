EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Coilcraft flyback breakout"
Date "18 May 2023"
Rev "Rev.1"
Comp "Remy Horton"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x05_Female J8
U 1 1 646833BA
P 5000 4250
F 0 "J8" V 4892 4498 50  0000 L CNN
F 1 "Conn_01x05_Female" H 5028 4185 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 5000 4250 50  0001 C CNN
F 3 "~" H 5000 4250 50  0001 C CNN
	1    5000 4250
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x05_Female J5
U 1 1 64683872
P 5000 3200
F 0 "J5" V 4892 2912 50  0000 R CNN
F 1 "Conn_01x05_Female" H 5028 3135 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 5000 3200 50  0001 C CNN
F 3 "~" H 5000 3200 50  0001 C CNN
	1    5000 3200
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J2
U 1 1 6468518F
P 4800 3850
F 0 "J2" V 4850 3900 50  0000 R CNN
F 1 "Conn_01x01_Female" H 4828 3785 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 4800 3850 50  0001 C CNN
F 3 "~" H 4800 3850 50  0001 C CNN
	1    4800 3850
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J1
U 1 1 64685483
P 4800 3600
F 0 "J1" V 4850 3650 50  0000 R CNN
F 1 "Conn_01x01_Female" H 4828 3535 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 4800 3600 50  0001 C CNN
F 3 "~" H 4800 3600 50  0001 C CNN
	1    4800 3600
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J4
U 1 1 64685C1B
P 4900 3850
F 0 "J4" V 4950 3900 50  0000 R CNN
F 1 "Conn_01x01_Female" H 4928 3785 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 4900 3850 50  0001 C CNN
F 3 "~" H 4900 3850 50  0001 C CNN
	1    4900 3850
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J7
U 1 1 64685E26
P 5000 3850
F 0 "J7" V 5050 3900 50  0000 R CNN
F 1 "Conn_01x01_Female" H 5028 3785 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 5000 3850 50  0001 C CNN
F 3 "~" H 5000 3850 50  0001 C CNN
	1    5000 3850
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J10
U 1 1 6468608A
P 5100 3850
F 0 "J10" V 5150 3900 50  0000 R CNN
F 1 "Conn_01x01_Female" H 5128 3785 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 5100 3850 50  0001 C CNN
F 3 "~" H 5100 3850 50  0001 C CNN
	1    5100 3850
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J12
U 1 1 646861FE
P 5200 3850
F 0 "J12" V 5250 3900 50  0000 R CNN
F 1 "Conn_01x01_Female" H 5228 3785 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 5200 3850 50  0001 C CNN
F 3 "~" H 5200 3850 50  0001 C CNN
	1    5200 3850
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J3
U 1 1 6468686D
P 4900 3600
F 0 "J3" V 4950 3650 50  0000 R CNN
F 1 "Conn_01x01_Female" H 4928 3535 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 4900 3600 50  0001 C CNN
F 3 "~" H 4900 3600 50  0001 C CNN
	1    4900 3600
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J6
U 1 1 64686FB1
P 5000 3600
F 0 "J6" V 5050 3650 50  0000 R CNN
F 1 "Conn_01x01_Female" H 5028 3535 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 5000 3600 50  0001 C CNN
F 3 "~" H 5000 3600 50  0001 C CNN
	1    5000 3600
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J9
U 1 1 64687184
P 5100 3600
F 0 "J9" V 5150 3650 50  0000 R CNN
F 1 "Conn_01x01_Female" H 5128 3535 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 5100 3600 50  0001 C CNN
F 3 "~" H 5100 3600 50  0001 C CNN
	1    5100 3600
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J11
U 1 1 646873D9
P 5200 3600
F 0 "J11" V 5250 3650 50  0000 R CNN
F 1 "Conn_01x01_Female" H 5228 3535 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 5200 3600 50  0001 C CNN
F 3 "~" H 5200 3600 50  0001 C CNN
	1    5200 3600
	0    1    1    0   
$EndComp
$EndSCHEMATC
