Printed Circuit Boards
======================
These are the design files for PCBs that I have decided to make public, most of which were made during the 2020-2021 Covid-19 lockdown.
See the `Readme.md` within the sub-directories for details of individual board designs.

* ``jtag-micromatch`` - *JTAG MicroMaTch programming adapter*
* ``thermocouple`` - *Thermocouple sensor circuit*
* ``relay`` - *Power relay switch circuit*
* ``power-jack`` - *Regulated voltage Mini-PCB*
* ``power-switch`` - *Power relay switch circult (Rev.2)*
* ``thermocouple-i2c`` - *I2C-based thermocouple daughter-board*
* ``lpcusb`` - *LPC11Uxx USB mini-evaluation board*
* ``i2c-master`` - *USB-based I2C master*
* ``i2c-slave`` - *I2C slave (for testing)*
* ``joulethief`` - *"Joule thief" voltage booster*
* ``inductor-breakout`` - *Breakout PCB for SMD coupled inductors*
* ``power-relay-rev3`` - *Power relay switch circult (Rev.3)*
* ``pic16f1454`` - *PIC16F1454 USB mini-evaluation board*
* ``thermocouple-rev2`` - *Thermocouple sensor circuit* (Rev.2)
* ``eeprom`` - *Atmel `AT28C64B` EEPROM programmer*
* ``ac2ac`` - *Capacative dropper circuit*
* ``555`` - *50% duty cycle oscillator*
* ``74095`` - *Breakout for Myraa 74095 flyback transformer*
* ``Q4xxx`` - *Breakout for Coilcraft Q4xxx flyback transformer*
* ``programming-seat`` - *Programming seat for 14-pin PIC microcontrollers*

## Licencing
Unless stated otherwise individually the PCB designs are covered by v1.2 of the [CERN Open Hardware Licence][cern-ohl].

In short you are free to have copies of these PCBs fabricated but if you distribute them you also have to provide a link back to this repository. In addition by distributing any fabricated PCBs you accept any and all legal responsibilities related to product safety and fitness for purpose.


## Contact

`remy.horton` `(at)` `gmail.com`

[cern-ohl]: https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-1.2