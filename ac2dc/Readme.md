Ac to DC converter
==================
This is a capacitative dropper that is intended for converting circa 240-volt mains power into circa 6-volt direct current and is here as an appendix to a [website article][remy-article].
From testing it seems able to supply 50mA at 5 volts although if used to power anything voltage-sensitive then it would be advisable to place a linear voltage regulator between the output and any downstream circuitry.
The circuit was designed using KiCad `v5.1.x` and for safety it is advised to only power this circuit from a galvanically isolated power supply.

![Schematic](circuit.png)


## Components

| Pad    | Component                         | Manufacturer      | Part number
| :----: | :-------------------------------: | :---------------: | :---------:
| C2     | 2.2μF X2 capacitor                | Epcos             | `B32924C3225K000`
| C3     | 10μF 450v capacitor               | Multicomp         | `MCKSK450M100G17S`
| C4     | 0.1μF capacitor                   | Vishay            | `K104K15X7RF53H5`
| D1     | Bridge rectifier                  | Multicomp         |  `W04G`
| D2     | 6.2v zener diode                  | Multicomp         | `1N4735A`
| F1     | Fuse holder                       | Schurter          | `0031.8002`
| _n/a_  | One amp fuse                      | Littelfuse        | `0312001.MXP`
| J1     | Terminal block                    | Multicomp         | `MC000034`
| J2     | Pin receptacle                    | Multicomp         | `2212S-02SG-85`
| Q1     | NPN transistor                    | Multicomp         | `MPSA44`
| R1     | _Pad not used_                    | _n/a_             | _n/a_
| R2/R3  | 470kΩ resistor                    | Vishay            | `PR03000204703JAC00`
| R4     | 2kΩ resistor                      | TE Connectivity   | `LR0204F2K0`
| TH1    | 50Ω NTC Thermistor                | Yageo             | `N10SP050M`


## Licencing
See the [top-level Readme.md](../Readme.md) for licencing details.


## Contact

`remy.horton` `(at)` `gmail.com`


[remy-article]: https://www.remy.org.uk/elec.php?elec=1684623600


