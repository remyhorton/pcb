EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "High-Voltage Interface"
Date "30 Dec 2022"
Rev "Rev.1"
Comp "Remy Horton"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Fuse F1
U 1 1 63AE2732
P 1200 900
F 0 "F1" V 1150 1000 50  0000 L CNN
F 1 "Fuse" H 1260 855 50  0001 L CNN
F 2 "Fuse:Fuseholder_Cylinder-6.3x32mm_Schurter_0031-8002_Horizontal_Open" V 1130 900 50  0001 C CNN
F 3 "~" H 1200 900 50  0001 C CNN
	1    1200 900 
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 63AE2FB9
P 1550 1150
F 0 "C2" V 1400 1150 50  0000 C CNN
F 1 "C" V 1389 1150 50  0001 C CNN
F 2 "Capacitor_THT:C_Rect_L31.5mm_W13.0mm_P27.50mm_MKS4" H 1588 1000 50  0001 C CNN
F 3 "~" H 1550 1150 50  0001 C CNN
	1    1550 1150
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 63AE35F4
P 1550 900
F 0 "R2" V 1500 1050 50  0000 C CNN
F 1 "R" V 1434 900 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0617_L17.0mm_D6.0mm_P25.40mm_Horizontal" V 1480 900 50  0001 C CNN
F 3 "~" H 1550 900 50  0001 C CNN
	1    1550 900 
	0    1    1    0   
$EndComp
$Comp
L Device:D_Zener D2
U 1 1 63AE6706
P 2950 1200
F 0 "D2" V 2850 1200 50  0000 L CNN
F 1 "D_Zener" V 2995 1280 50  0001 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 2950 1200 50  0001 C CNN
F 3 "~" H 2950 1200 50  0001 C CNN
	1    2950 1200
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 63AF41AC
P 1200 1300
F 0 "R1" V 1150 1450 50  0000 C CNN
F 1 "R" V 1084 1300 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1130 1300 50  0001 C CNN
F 3 "~" H 1200 1300 50  0001 C CNN
	1    1200 1300
	0    1    1    0   
$EndComp
$Comp
L Device:Thermistor_NTC TH1
U 1 1 63AF47D6
P 1200 1500
F 0 "TH1" V 1300 1550 50  0000 C CNN
F 1 "Thermistor_NTC" V 1001 1500 50  0001 C CNN
F 2 "Varistor:RV_Disc_D12mm_W4.2mm_P7.5mm" H 1200 1550 50  0001 C CNN
F 3 "~" H 1200 1550 50  0001 C CNN
	1    1200 1500
	0    1    1    0   
$EndComp
$Comp
L Device:D_Bridge_+A-A D1
U 1 1 63EC7606
P 2000 1150
F 0 "D1" V 2000 1200 50  0000 R CNN
F 1 "D_Bridge_+A-A" V 1955 806 50  0001 R CNN
F 2 "Diode_THT:Diode_Bridge_Round_D9.8mm" H 2000 1150 50  0001 C CNN
F 3 "~" H 2000 1150 50  0001 C CNN
	1    2000 1150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1400 900  1400 1150
Wire Wire Line
	1700 900  1700 1150
$Comp
L Connector:Screw_Terminal_01x02 J1
U 1 1 63ED3DE4
P 1050 1050
F 0 "J1" H 1000 1150 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 1130 951 50  0001 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-2-5.08_1x02_P5.08mm_Horizontal" H 1050 1050 50  0001 C CNN
F 3 "~" H 1050 1050 50  0001 C CNN
	1    1050 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	850  900  850  1050
Wire Wire Line
	1050 900  850  900 
Wire Wire Line
	1400 900  1350 900 
Connection ~ 1400 900 
Connection ~ 1700 1150
Wire Wire Line
	1350 1500 2300 1500
Wire Wire Line
	2300 1500 2300 1150
Wire Wire Line
	850  1150 850  1300
Wire Wire Line
	850  1500 1050 1500
Wire Wire Line
	1050 1300 850  1300
Connection ~ 850  1300
Wire Wire Line
	850  1300 850  1500
Wire Wire Line
	1350 1300 1350 1500
Connection ~ 1350 1500
$Comp
L Device:R R3
U 1 1 63EE62B7
P 2650 1150
F 0 "R3" H 2720 1196 50  0000 L CNN
F 1 "R" H 2720 1105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0617_L17.0mm_D6.0mm_P25.40mm_Horizontal" V 2580 1150 50  0001 C CNN
F 3 "~" H 2650 1150 50  0001 C CNN
	1    2650 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 63EE6901
P 2450 1150
F 0 "C3" H 2500 1250 50  0000 L CNN
F 1 "C" H 2500 1050 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 2488 1000 50  0001 C CNN
F 3 "~" H 2450 1150 50  0001 C CNN
	1    2450 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 1450 2450 1450
Wire Wire Line
	2450 1450 2450 1300
Wire Wire Line
	2000 850  2450 850 
Wire Wire Line
	2450 850  2450 1000
Wire Wire Line
	2650 1000 2650 850 
Wire Wire Line
	2650 850  2450 850 
Connection ~ 2450 850 
Wire Wire Line
	2450 1450 2650 1450
Wire Wire Line
	2650 1450 2650 1300
Connection ~ 2450 1450
$Comp
L Device:Q_NPN_CBE Q1
U 1 1 63EE9654
P 3350 1050
F 0 "Q1" H 3540 1050 50  0000 L CNN
F 1 "Q_NPN_CBE" H 3541 1005 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 3550 1150 50  0001 C CNN
F 3 "~" H 3350 1050 50  0001 C CNN
	1    3350 1050
	1    0    0    -1  
$EndComp
Connection ~ 2650 1000
$Comp
L Device:R R4
U 1 1 63EE7ECD
P 2800 1000
F 0 "R4" V 2700 1100 50  0000 C CNN
F 1 "R" V 2700 900 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 2730 1000 50  0001 C CNN
F 3 "~" H 2800 1000 50  0001 C CNN
	1    2800 1000
	0    1    1    0   
$EndComp
Connection ~ 2650 850 
Wire Wire Line
	2950 1000 2950 1050
$Comp
L Device:C C4
U 1 1 63EED0B3
P 3100 1200
F 0 "C4" H 3100 1300 50  0000 L CNN
F 1 "C" H 3100 1100 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 3138 1050 50  0001 C CNN
F 3 "~" H 3100 1200 50  0001 C CNN
	1    3100 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 1050 3100 1050
Connection ~ 2950 1050
Wire Wire Line
	2650 1450 2950 1450
Wire Wire Line
	2950 1450 2950 1350
Connection ~ 2650 1450
Wire Wire Line
	2950 1450 3100 1450
Wire Wire Line
	3100 1450 3100 1350
Connection ~ 2950 1450
Connection ~ 3100 1050
Wire Wire Line
	3150 1050 3100 1050
Wire Wire Line
	2650 850  3450 850 
$Comp
L Connector:Screw_Terminal_01x02 J2
U 1 1 63EF0BA1
P 3650 1250
F 0 "J2" H 3730 1196 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 3730 1151 50  0001 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x01_P2.54mm_Vertical" H 3650 1250 50  0001 C CNN
F 3 "~" H 3650 1250 50  0001 C CNN
	1    3650 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 1450 3450 1450
Wire Wire Line
	3450 1450 3450 1350
Connection ~ 3100 1450
$EndSCHEMATC
