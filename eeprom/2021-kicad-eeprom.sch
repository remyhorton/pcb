EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "AT28C64B EEPROM Programmer"
Date "30 April 2021"
Rev "Rev.1"
Comp "Remy Horton"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Microchip_PIC16:PIC16F1829-IP U3
U 1 1 608BB787
P 8350 5400
F 0 "U3" H 9250 6200 50  0000 C CNN
F 1 "PIC16F1829-IP" H 7700 6200 50  0000 C CNN
F 2 "Package_DIP:DIP-20_W7.62mm" H 8350 4850 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/41440C.pdf" H 8350 4850 50  0001 C CNN
	1    8350 5400
	1    0    0    -1  
$EndComp
$Comp
L Interface_Expansion:PCF8574 U2
U 1 1 608BC3CE
P 5550 2900
F 0 "U2" H 5200 3550 50  0000 C CNN
F 1 "PCF8574" H 5800 3550 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 5550 2900 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/PCF8574_PCF8574A.pdf" H 5550 2900 50  0001 C CNN
	1    5550 2900
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC595 U1
U 1 1 608BC864
P 5900 5150
F 0 "U1" H 6150 5700 50  0000 C CNN
F 1 "74HC595" H 5650 5700 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 5900 5150 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74hc595.pdf" H 5900 5150 50  0001 C CNN
	1    5900 5150
	1    0    0    -1  
$EndComp
$Comp
L AT28C64B:AT28C64B U4
U 1 1 608C0902
P 10400 5350
F 0 "U4" V 9525 5350 50  0000 C CNN
F 1 "AT28C64B" V 9616 5350 50  0000 C CNN
F 2 "Socket:DIP_Socket-28_W11.9_W12.7_W15.24_W17.78_W18.5_3M_228-1277-00-0602J" H 10400 5350 50  0001 C CNN
F 3 "" H 10400 5350 50  0001 C CNN
	1    10400 5350
	0    1    1    0   
$EndComp
Text GLabel 10050 5600 0    50   Input ~ 0
Addr0
Text GLabel 10050 5500 0    50   Input ~ 0
Addr1
Text GLabel 10050 5400 0    50   Input ~ 0
Addr2
Text GLabel 10050 5300 0    50   Input ~ 0
Addr3
Text GLabel 10050 5200 0    50   Input ~ 0
Addr4
Text GLabel 10050 5100 0    50   Input ~ 0
Addr5
Text GLabel 6300 4750 2    50   Input ~ 0
Addr0
Text GLabel 10050 4900 0    50   Input ~ 0
Addr7
Text GLabel 10750 5000 2    50   Input ~ 0
Addr8
Text GLabel 10750 5100 2    50   Input ~ 0
Addr9
Text GLabel 10750 5400 2    50   Input ~ 0
Addr10
Text GLabel 10750 5200 2    50   Input ~ 0
Addr11
Text GLabel 10050 4800 0    50   Input ~ 0
Addr12
$Comp
L Device:R R1
U 1 1 608D878F
P 4850 1500
F 0 "R1" H 4920 1546 50  0000 L CNN
F 1 "10k" H 4920 1455 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4780 1500 50  0001 C CNN
F 3 "~" H 4850 1500 50  0001 C CNN
	1    4850 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 608DB7B6
P 5100 1500
F 0 "R2" H 5170 1546 50  0000 L CNN
F 1 "10k" H 5170 1455 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5030 1500 50  0001 C CNN
F 3 "~" H 5100 1500 50  0001 C CNN
	1    5100 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 608DBAB9
P 5350 1500
F 0 "R3" H 5420 1546 50  0000 L CNN
F 1 "10k" H 5420 1455 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5280 1500 50  0001 C CNN
F 3 "~" H 5350 1500 50  0001 C CNN
	1    5350 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 608DBD01
P 5600 1500
F 0 "R4" H 5670 1546 50  0000 L CNN
F 1 "10k" H 5670 1455 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5530 1500 50  0001 C CNN
F 3 "~" H 5600 1500 50  0001 C CNN
	1    5600 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 608DBFA8
P 5850 1500
F 0 "R5" H 5920 1546 50  0000 L CNN
F 1 "10k" H 5920 1455 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5780 1500 50  0001 C CNN
F 3 "~" H 5850 1500 50  0001 C CNN
	1    5850 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 608DC293
P 6100 1500
F 0 "R6" H 6170 1546 50  0000 L CNN
F 1 "10k" H 6170 1455 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 6030 1500 50  0001 C CNN
F 3 "~" H 6100 1500 50  0001 C CNN
	1    6100 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 1350 5850 1350
Connection ~ 5850 1350
Wire Wire Line
	5600 1350 5850 1350
Wire Wire Line
	5100 1350 5350 1350
Wire Wire Line
	4850 1350 5100 1350
Connection ~ 5100 1350
$Comp
L power:VCC #PWR0101
U 1 1 608DD42D
P 5600 1350
F 0 "#PWR0101" H 5600 1200 50  0001 C CNN
F 1 "VCC" H 5615 1523 50  0000 C CNN
F 2 "" H 5600 1350 50  0001 C CNN
F 3 "" H 5600 1350 50  0001 C CNN
	1    5600 1350
	1    0    0    -1  
$EndComp
Text GLabel 6300 4850 2    50   Input ~ 0
Addr1
Text GLabel 6300 4950 2    50   Input ~ 0
Addr2
Text GLabel 6300 5050 2    50   Input ~ 0
Addr3
Text GLabel 6300 5150 2    50   Input ~ 0
Addr4
Text GLabel 6300 5250 2    50   Input ~ 0
Addr5
Text GLabel 10050 5000 0    50   Input ~ 0
Addr6
Text GLabel 4850 1650 3    50   Input ~ 0
Addr6
Text GLabel 5100 1650 3    50   Input ~ 0
Addr7
Text GLabel 5350 1650 3    50   Input ~ 0
Addr8
Text GLabel 5600 1650 3    50   Input ~ 0
Addr9
Text GLabel 5850 1650 3    50   Input ~ 0
Addr10
Text GLabel 6100 1650 3    50   Input ~ 0
Addr11
$Comp
L Device:R R7
U 1 1 608FAD3F
P 6350 1500
F 0 "R7" H 6420 1546 50  0000 L CNN
F 1 "10k" H 6420 1455 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 6280 1500 50  0001 C CNN
F 3 "~" H 6350 1500 50  0001 C CNN
	1    6350 1500
	1    0    0    -1  
$EndComp
Text GLabel 6350 1650 3    50   Input ~ 0
Addr12
Wire Wire Line
	5350 1350 5600 1350
Connection ~ 5350 1350
Connection ~ 5600 1350
Wire Wire Line
	6350 1350 6100 1350
Connection ~ 6100 1350
Text GLabel 10750 5700 2    50   Input ~ 0
Data6
Text GLabel 10750 5800 2    50   Input ~ 0
Data5
Text GLabel 10750 5900 2    50   Input ~ 0
Data4
Text GLabel 10750 6000 2    50   Input ~ 0
Data3
Text GLabel 10750 5600 2    50   Input ~ 0
Data7
Text GLabel 9450 5700 2    50   Input ~ 0
Data0
Text GLabel 9450 5600 2    50   Input ~ 0
Data1
Text GLabel 9450 5500 2    50   Input ~ 0
Data2
Text GLabel 9450 5400 2    50   Input ~ 0
Data3
Text GLabel 9450 5300 2    50   Input ~ 0
Data4
Text GLabel 9450 5200 2    50   Input ~ 0
Data5
Text GLabel 9450 5100 2    50   Input ~ 0
Data6
Text GLabel 9450 5000 2    50   Input ~ 0
Data7
Text GLabel 10050 5700 0    50   Input ~ 0
Data0
Text GLabel 10050 5800 0    50   Input ~ 0
Data1
Text GLabel 10050 5900 0    50   Input ~ 0
Data2
Text GLabel 10750 5300 2    50   Input ~ 0
OutEn
Text GLabel 10750 4800 2    50   Input ~ 0
WriteEn
$Comp
L power:VCC #PWR0102
U 1 1 609306AC
P 10850 4650
F 0 "#PWR0102" H 10850 4500 50  0001 C CNN
F 1 "VCC" H 10865 4823 50  0000 C CNN
F 2 "" H 10850 4650 50  0001 C CNN
F 3 "" H 10850 4650 50  0001 C CNN
	1    10850 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	10750 4700 10850 4700
Wire Wire Line
	10850 4700 10850 4650
$Comp
L power:GND #PWR0103
U 1 1 609311BF
P 10050 6000
F 0 "#PWR0103" H 10050 5750 50  0001 C CNN
F 1 "GND" H 10055 5827 50  0000 C CNN
F 2 "" H 10050 6000 50  0001 C CNN
F 3 "" H 10050 6000 50  0001 C CNN
	1    10050 6000
	1    0    0    -1  
$EndComp
Text GLabel 5500 4950 0    50   Input ~ 0
Clock
Text GLabel 5500 5250 0    50   Input ~ 0
Load
NoConn ~ 6300 5450
NoConn ~ 6300 5650
Text GLabel 5500 4750 0    50   Input ~ 0
Serial
$Comp
L power:GND #PWR0104
U 1 1 60946F49
P 5900 5850
F 0 "#PWR0104" H 5900 5600 50  0001 C CNN
F 1 "GND" H 5905 5677 50  0000 C CNN
F 2 "" H 5900 5850 50  0001 C CNN
F 3 "" H 5900 5850 50  0001 C CNN
	1    5900 5850
	1    0    0    -1  
$EndComp
Text GLabel 6050 2500 2    50   Input ~ 0
Addr6
Text GLabel 6050 2600 2    50   Input ~ 0
Addr7
Text GLabel 6050 2700 2    50   Input ~ 0
Addr8
Text GLabel 6050 2800 2    50   Input ~ 0
Addr9
Text GLabel 6050 2900 2    50   Input ~ 0
Addr10
Text GLabel 6050 3000 2    50   Input ~ 0
Addr11
Text GLabel 6050 3100 2    50   Input ~ 0
Addr12
Wire Wire Line
	5050 2800 5050 2900
Wire Wire Line
	5050 2900 5050 3000
Connection ~ 5050 2900
Text GLabel 7250 5600 0    50   Input ~ 0
SDA
Text GLabel 5050 2600 0    50   Input ~ 0
SDA
Text GLabel 5050 2500 0    50   Input ~ 0
SCL
$Comp
L power:VCC #PWR0105
U 1 1 6095764A
P 5550 2150
F 0 "#PWR0105" H 5550 2000 50  0001 C CNN
F 1 "VCC" H 5565 2323 50  0000 C CNN
F 2 "" H 5550 2150 50  0001 C CNN
F 3 "" H 5550 2150 50  0001 C CNN
	1    5550 2150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 6095820A
P 5550 3600
F 0 "#PWR0106" H 5550 3350 50  0001 C CNN
F 1 "GND" H 5555 3427 50  0000 C CNN
F 2 "" H 5550 3600 50  0001 C CNN
F 3 "" H 5550 3600 50  0001 C CNN
	1    5550 3600
	1    0    0    -1  
$EndComp
NoConn ~ 5050 3300
$Comp
L Connector:Conn_PIC_ICSP_ICD J1
U 1 1 60962803
P 7450 3050
F 0 "J1" H 7121 3096 50  0000 R CNN
F 1 "Conn_PIC_ICSP_ICD" H 8050 3400 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x06_P2.54mm_Vertical" H 7500 3200 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/devicedoc/30277d.pdf" V 7150 2900 50  0001 C CNN
	1    7450 3050
	1    0    0    -1  
$EndComp
Text GLabel 7950 3050 2    50   Input ~ 0
PGD
Text GLabel 7950 3150 2    50   Input ~ 0
PGC
Text GLabel 7950 2850 2    50   Input ~ 0
MCLR
$Comp
L power:GND #PWR0107
U 1 1 60963B74
P 7250 3450
F 0 "#PWR0107" H 7250 3200 50  0001 C CNN
F 1 "GND" H 7255 3277 50  0000 C CNN
F 2 "" H 7250 3450 50  0001 C CNN
F 3 "" H 7250 3450 50  0001 C CNN
	1    7250 3450
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0108
U 1 1 60963F04
P 7250 2650
F 0 "#PWR0108" H 7250 2500 50  0001 C CNN
F 1 "VCC" H 7265 2823 50  0000 C CNN
F 2 "" H 7250 2650 50  0001 C CNN
F 3 "" H 7250 2650 50  0001 C CNN
	1    7250 2650
	1    0    0    -1  
$EndComp
NoConn ~ 7950 3250
Text GLabel 7250 5200 0    50   Input ~ 0
MCLR
Text GLabel 7200 4850 1    50   Input ~ 0
PGC
Text GLabel 7100 4850 1    50   Input ~ 0
PGD
Text GLabel 7250 5800 0    50   Input ~ 0
SCL
Text GLabel 7250 5900 0    50   Input ~ 0
Tx
Text GLabel 7250 5700 0    50   Input ~ 0
Rx
Text GLabel 7250 5300 0    50   Input ~ 0
WriteEn
Text GLabel 7250 5400 0    50   Input ~ 0
OutEn
Text GLabel 7050 4900 0    50   Input ~ 0
Serial
Text GLabel 7050 5000 0    50   Input ~ 0
Clock
Text GLabel 7250 5100 0    50   Input ~ 0
Load
$Comp
L power:GND #PWR0109
U 1 1 609EF77C
P 8350 6200
F 0 "#PWR0109" H 8350 5950 50  0001 C CNN
F 1 "GND" H 8355 6027 50  0000 C CNN
F 2 "" H 8350 6200 50  0001 C CNN
F 3 "" H 8350 6200 50  0001 C CNN
	1    8350 6200
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0110
U 1 1 609F60BC
P 8350 4500
F 0 "#PWR0110" H 8350 4350 50  0001 C CNN
F 1 "VCC" H 8365 4673 50  0000 C CNN
F 2 "" H 8350 4500 50  0001 C CNN
F 3 "" H 8350 4500 50  0001 C CNN
	1    8350 4500
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0111
U 1 1 609F6F8B
P 5900 4500
F 0 "#PWR0111" H 5900 4350 50  0001 C CNN
F 1 "VCC" H 5915 4673 50  0000 C CNN
F 2 "" H 5900 4500 50  0001 C CNN
F 3 "" H 5900 4500 50  0001 C CNN
	1    5900 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 4500 5200 4500
Wire Wire Line
	5200 4500 5200 5050
Wire Wire Line
	5200 5050 5500 5050
Wire Wire Line
	5900 4550 5900 4500
Connection ~ 5900 4500
Wire Wire Line
	5500 5350 5500 5850
Wire Wire Line
	5500 5850 5900 5850
Connection ~ 5900 5850
Wire Wire Line
	7250 5000 7200 5000
Wire Wire Line
	7250 4900 7100 4900
Wire Wire Line
	7100 4850 7100 4900
Connection ~ 7100 4900
Wire Wire Line
	7100 4900 7050 4900
Wire Wire Line
	7200 4850 7200 5000
Connection ~ 7200 5000
Wire Wire Line
	7200 5000 7050 5000
$Comp
L power:VCC #PWR0112
U 1 1 60A0A286
P 8300 900
F 0 "#PWR0112" H 8300 750 50  0001 C CNN
F 1 "VCC" H 8315 1073 50  0000 C CNN
F 2 "" H 8300 900 50  0001 C CNN
F 3 "" H 8300 900 50  0001 C CNN
	1    8300 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 2150 4800 2150
Wire Wire Line
	4800 2150 4800 2900
Wire Wire Line
	4800 2900 5050 2900
Wire Wire Line
	5550 2200 5550 2150
Connection ~ 5550 2150
NoConn ~ 6300 5350
$Comp
L Connector_Generic:Conn_01x03 J2
U 1 1 60A19388
P 8550 1750
F 0 "J2" V 8468 1562 50  0000 R CNN
F 1 "Conn_01x03" V 8423 1562 50  0001 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 8550 1750 50  0001 C CNN
F 3 "~" H 8550 1750 50  0001 C CNN
	1    8550 1750
	0    -1   -1   0   
$EndComp
$Comp
L Interface_USB:FT230XS U5
U 1 1 60A19C48
P 10300 1400
F 0 "U5" H 9800 2000 50  0000 C CNN
F 1 "FT230XS" H 10700 2000 50  0000 C CNN
F 2 "Package_SO:SSOP-16_3.9x4.9mm_P0.635mm" H 11300 800 50  0001 C CNN
F 3 "https://www.ftdichip.com/Support/Documents/DataSheets/ICs/DS_FT230X.pdf" H 10300 1400 50  0001 C CNN
	1    10300 1400
	1    0    0    -1  
$EndComp
Text GLabel 11000 1000 2    50   Input ~ 0
Rx
Text GLabel 11000 1100 2    50   Input ~ 0
Tx
NoConn ~ 11000 1200
NoConn ~ 11000 1300
NoConn ~ 11000 1500
NoConn ~ 11000 1600
NoConn ~ 11000 1700
NoConn ~ 11000 1800
$Comp
L power:GND #PWR02
U 1 1 60A1D2DD
P 9100 2100
F 0 "#PWR02" H 9100 1850 50  0001 C CNN
F 1 "GND" H 9105 1927 50  0000 C CNN
F 2 "" H 9100 2100 50  0001 C CNN
F 3 "" H 9100 2100 50  0001 C CNN
	1    9100 2100
	1    0    0    -1  
$EndComp
Text GLabel 8650 1950 3    50   Input ~ 0
Rx
Text GLabel 8550 1950 3    50   Input ~ 0
Tx
$Comp
L power:GND #PWR01
U 1 1 60A255FF
P 8450 1950
F 0 "#PWR01" H 8450 1700 50  0001 C CNN
F 1 "GND" H 8455 1777 50  0000 C CNN
F 2 "" H 8450 1950 50  0001 C CNN
F 3 "" H 8450 1950 50  0001 C CNN
	1    8450 1950
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B_Mini J3
U 1 1 60A2D3B5
P 8600 1150
F 0 "J3" H 8657 1617 50  0000 C CNN
F 1 "USB_B_Mini" H 8657 1526 50  0000 C CNN
F 2 "Connector_USB:USB_Mini-B_Wuerth_65100516121_Horizontal" H 8750 1100 50  0001 C CNN
F 3 "~" H 8750 1100 50  0001 C CNN
	1    8600 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 700  8900 950 
Wire Wire Line
	9600 1000 9600 650 
Wire Wire Line
	9600 650  10400 650 
Wire Wire Line
	10400 650  10400 700 
$Comp
L Device:C C2
U 1 1 60A35AC2
P 9300 850
F 0 "C2" H 9350 950 50  0000 L CNN
F 1 "100nF" H 9350 750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9338 700 50  0001 C CNN
F 3 "~" H 9300 850 50  0001 C CNN
	1    9300 850 
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 60A357F0
P 9100 850
F 0 "C1" H 8950 950 50  0000 L CNN
F 1 "4.7uF" H 8900 750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9138 700 50  0001 C CNN
F 3 "~" H 9100 850 50  0001 C CNN
	1    9100 850 
	1    0    0    -1  
$EndComp
Connection ~ 9100 700 
Wire Wire Line
	9100 700  8900 700 
Connection ~ 9300 700 
Wire Wire Line
	9100 700  9300 700 
Wire Wire Line
	9300 700  10200 700 
Wire Wire Line
	9600 1300 8950 1300
Wire Wire Line
	8950 1300 8950 1250
Wire Wire Line
	8950 1250 8900 1250
NoConn ~ 8900 1350
Wire Wire Line
	8600 1550 9100 1550
Wire Wire Line
	9100 1550 9100 1000
Wire Wire Line
	9300 1000 9100 1000
Connection ~ 9100 1000
Wire Wire Line
	9600 1600 9500 1600
Wire Wire Line
	9500 1600 9500 1050
Wire Wire Line
	9500 1050 9600 1050
Wire Wire Line
	9600 1050 9600 1000
Connection ~ 9600 1000
Wire Wire Line
	9250 1400 9250 1150
Wire Wire Line
	9250 1150 8900 1150
Wire Wire Line
	9250 1400 9600 1400
Wire Wire Line
	10200 2100 9100 2100
Wire Wire Line
	9100 2100 9100 1550
Connection ~ 9100 1550
Wire Wire Line
	10400 2100 10200 2100
Connection ~ 10200 2100
Connection ~ 9100 2100
NoConn ~ 8500 1550
$Comp
L Regulator_Linear:uA7805 U6
U 1 1 60A5E726
P 8000 900
F 0 "U6" H 8000 1142 50  0000 C CNN
F 1 "uA7805" H 8000 1051 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 8025 750 50  0001 L CIN
F 3 "http://www.ti.com/lit/ds/symlink/ua78.pdf" H 8000 850 50  0001 C CNN
	1    8000 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 1100 7700 1200
Wire Wire Line
	7700 1200 8000 1200
$Comp
L power:GND #PWR0113
U 1 1 60A741A9
P 8000 1200
F 0 "#PWR0113" H 8000 950 50  0001 C CNN
F 1 "GND" H 8005 1027 50  0000 C CNN
F 2 "" H 8000 1200 50  0001 C CNN
F 3 "" H 8000 1200 50  0001 C CNN
	1    8000 1200
	1    0    0    -1  
$EndComp
Connection ~ 8000 1200
Wire Wire Line
	10050 6000 9750 6000
Connection ~ 10050 6000
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 60A95894
P 7700 850
F 0 "#FLG0101" H 7700 925 50  0001 C CNN
F 1 "PWR_FLAG" H 7700 1023 50  0000 C CNN
F 2 "" H 7700 850 50  0001 C CNN
F 3 "~" H 7700 850 50  0001 C CNN
	1    7700 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 850  7700 900 
Wire Bus Line
	7150 600  7150 2350
Wire Bus Line
	7150 2350 11200 2350
Wire Bus Line
	11200 2350 11200 600 
Wire Bus Line
	7150 600  11200 600 
$Comp
L Device:LED D1
U 1 1 60AAF485
P 6500 2550
F 0 "D1" V 6539 2432 50  0000 R CNN
F 1 "LED" V 6448 2432 50  0000 R CNN
F 2 "LED_THT:LED_Rectangular_W5.0mm_H2.0mm" H 6500 2550 50  0001 C CNN
F 3 "~" H 6500 2550 50  0001 C CNN
	1    6500 2550
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R8
U 1 1 60AB002A
P 6500 2850
F 0 "R8" H 6570 2896 50  0000 L CNN
F 1 "1k" H 6570 2805 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6430 2850 50  0001 C CNN
F 3 "~" H 6500 2850 50  0001 C CNN
	1    6500 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 3200 6500 3200
Wire Wire Line
	6500 3200 6500 3000
Wire Wire Line
	5550 2150 6500 2150
Wire Wire Line
	6500 2150 6500 2400
$Comp
L Device:R R9
U 1 1 60AD80EF
P 9300 3050
F 0 "R9" H 9370 3096 50  0000 L CNN
F 1 "10k" H 9370 3005 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 9230 3050 50  0001 C CNN
F 3 "~" H 9300 3050 50  0001 C CNN
	1    9300 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 1 60AD8528
P 9600 3050
F 0 "R10" H 9670 3096 50  0000 L CNN
F 1 "10k" H 9670 3005 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 9530 3050 50  0001 C CNN
F 3 "~" H 9600 3050 50  0001 C CNN
	1    9600 3050
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR03
U 1 1 60AD900B
P 9300 2900
F 0 "#PWR03" H 9300 2750 50  0001 C CNN
F 1 "VCC" H 9315 3073 50  0000 C CNN
F 2 "" H 9300 2900 50  0001 C CNN
F 3 "" H 9300 2900 50  0001 C CNN
	1    9300 2900
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR04
U 1 1 60AD9531
P 9600 2900
F 0 "#PWR04" H 9600 2750 50  0001 C CNN
F 1 "VCC" H 9615 3073 50  0000 C CNN
F 2 "" H 9600 2900 50  0001 C CNN
F 3 "" H 9600 2900 50  0001 C CNN
	1    9600 2900
	1    0    0    -1  
$EndComp
Text GLabel 9300 3200 3    50   Input ~ 0
SCL
Text GLabel 9600 3200 3    50   Input ~ 0
SDA
$Comp
L Connector:Barrel_Jack J4
U 1 1 60ADE1DD
P 7400 1000
F 0 "J4" H 7457 1325 50  0000 C CNN
F 1 "Barrel_Jack" H 7457 1234 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_CUI_PJ-102AH_Horizontal" H 7450 960 50  0001 C CNN
F 3 "~" H 7450 960 50  0001 C CNN
	1    7400 1000
	1    0    0    -1  
$EndComp
Connection ~ 7700 900 
Wire Wire Line
	9750 6000 9750 6250
Wire Wire Line
	9750 6250 11100 6250
Wire Wire Line
	11100 6250 11100 5500
Wire Wire Line
	11100 5500 10750 5500
$Comp
L power:VCC #PWR05
U 1 1 60B19741
P 8650 2900
F 0 "#PWR05" H 8650 2750 50  0001 C CNN
F 1 "VCC" H 8665 3073 50  0000 C CNN
F 2 "" H 8650 2900 50  0001 C CNN
F 3 "" H 8650 2900 50  0001 C CNN
	1    8650 2900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 60B19F1C
P 8650 3200
F 0 "#PWR06" H 8650 2950 50  0001 C CNN
F 1 "GND" H 8655 3027 50  0000 C CNN
F 2 "" H 8650 3200 50  0001 C CNN
F 3 "" H 8650 3200 50  0001 C CNN
	1    8650 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 60B1A548
P 8650 3050
F 0 "C3" H 8765 3096 50  0000 L CNN
F 1 "100nF" H 8765 3005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8688 2900 50  0001 C CNN
F 3 "~" H 8650 3050 50  0001 C CNN
	1    8650 3050
	1    0    0    -1  
$EndComp
$EndSCHEMATC
