EEPROM programmer
=================
This is a basic reader/writer for the 28-pin _Atmel_ `AT28C64B` EEPROM which is based around the `PIC16F1829` microcontroller.
It is an appendix to a [website article][remy-article] which contains the background and technical details, including links to the associated firmware.
It was made using KiCad `v5.1.x`, although I am unsure which version of the KiCAD libraries it used since the workstation it was made on has since been decomissioned.

![Schematic](circuit.jpeg)


## Components

| Pad    | Component                         | Manufacturer      | Part number
| :----: | :-------------------------------: | :---------------: | :---------:
| U1     | Serial-to-parallel                | Texas Instruments | `SN74HC595N`
| U2     | I/O Expander                      | Texas Instruments | `PCF8574`
| U3     | Microcontroller                   | Microchip         | `PIC16F1454`
| U4     | Zero-force socket
| U5     | RS232-USB converter               | FTDI              | `FT230XS`
| U6     | 5-volt power regulator            | Texas Instruments | `UA78M05IDCYR`
| J1     | 6-way receptacle                  | Multicomp         | `2212S-06SG-85`
| J2     | Pin header strip                  | Multicomp         | `2211S-22G`
| J3     | Mini-USB connector                | Wurth             | `65100516121`
| J4     | Barrel jack                       | CUI Devices       | `PJ-102AH`
| D1     | Through-hole Red LED              | Kingbright        | `L-113IDT`
| R1-R7  | 10kΩ pull-up resisitor
| R9-R10 | 10kΩ pull-up resisitor
| R8     | 1.5kΩ protective resisitor
| C1     | 4.7μF capacitor
| C2-C3  | 100nF capacitor


## Licencing
See the [top-level Readme.md](../Readme.md) for licencing details.


## Contact

`remy.horton` `(at)` `gmail.com`


[remy-article]: https://www.remy.org.uk/elec.php?elec=1632610800


