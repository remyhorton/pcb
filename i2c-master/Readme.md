USB-based I2C Master, Rev.2
===========================
A USB powered and driven I2C master adapter. It is [a reedesign][article]
of an earlier adapter I made two years ago in response to an incompatibility
between an off-the-shelf adapter and some hardware I was testing at the time.


## Circuit schematic

![Schematic](schematic.png)


## Components

| Pad   | Component               | Manufacturer     | Part number
| :---: | :---------------------: | :--------------: | :--------------:
| U1    | Microcontroller         | Microchip        | `PIC12LF1840`
| U2    | USB-RS232 converter     | FTDI             | `FT230XS`
| R1,R2 | 27Ω resistor            | Multicomp        | `MCWR08X27R0FTL`
| R3    | 1kΩ protective resistor | Multicomp        | `MCWR08X1001FTL`
| R4,R5 | 10kΩ pull-up resisitor  | Multicomp        | `MCWR12X1002`
| D1    | Indicator LED           | Osram            | `LS R976`
| C1,C4 | 0.1μF bypass capacitor  | Samsung          | `CL21B104KACNNNC`
| C2,C3 | 47pF bypass capacitor   |
| C5 	| 4.7μF bypass capacitor  |
| J1 	| 4-way receptacle        |	Multicomp        | `2212S-04SG-85`
| JP1 	| Pin header              | Multicomp        | `2211S-22G`
| J3    | USB Mini-B connector    | Wurth            | `65100516121`


## Licencing
See the [top-level Readme.md](../Readme.md) for licencing details.


## Contact

`remy.horton` `(at)` `gmail.com`


[article]: http://www.remy.org.uk