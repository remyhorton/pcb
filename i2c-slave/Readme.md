PIC16F1823 Breakout, Rev.1
==========================
This circuit is a variant of the [I2C Master](../i2c-master), the main difference
being external power rather than drawing current from the USB VBus. It was
originally designed as a testing I2C slave device for the former, and includes
KiCad symbol for the `PIC16F1823` since there is not currently a built-in one.


## Circuit schematic

![Schematic](schematic.png)


## Components

| Pad   | Component                | Manufacturer     | Part number
| :---: | :---------------------:  | :--------------: | :--------------:
| U1    | 5v power regulator       | 
| U2    | Microcontroller          | Microchip        | `PIC16F1823-I/SL`
| U3    | USB-RS232 converter      | FTDI             | `FT230XS`
| R1    | 1k protective resistor   | Multicomp        | `MCWR08X1001FTL`
| R2,R3 | 27R USB resistor         | Multicomp        | `MCWR08X27R0FTL`
| D1    | Indicator LED            | Osram            | `LS R976`
| C1,C2 | 0.1uF bypass capacitor   | Samsung          | `CL21B104KACNNNC`
| J1    | Barrel Jack              |
| J4 	| Pin header               | Multicomp        | `2211S-22G`
| J5 	| 6-way receptacle         | Multicomp        |


## Licencing
See the [top-level Readme.md](../Readme.md) for licencing details.


## Contact

`remy.horton` `(at)` `gmail.com`
