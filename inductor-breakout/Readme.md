Coupled inductor breakout board
===============================
A breakout PCB for dual coupled inductors in a 12.5x12.5mm case.

![Rendering](rendering.png)


## Circuit schematic

![Schematic](schematic.png)


## Components

| Pad num  | Component              | Manufacturer | Part number
| :------: | :--------------------: | :----------: | :---------:
| `L1`     | Dual-coupled inductor  | Bourns       | `SRF1260-101M`
| `J1`     | 4-way receptacle       | Multicomp    | `2212S-04SG-85`


## Licencing
See the [top-level Readme.md](../Readme.md) for licencing details.


## Contact

`remy.horton` `(at)` `gmail.com`