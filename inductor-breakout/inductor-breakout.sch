EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Coupled Inductor Breakout"
Date "13 November 2020"
Rev "Rev.1"
Comp "Remy Horton"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:L_Core_Ferrite_Coupled_1342 L1
U 1 1 5FAEAF31
P 2300 2450
F 0 "L1" H 2300 2600 50  0000 C CNN
F 1 "L_Core_Ferrite_Coupled_1342" H 2300 2640 50  0001 C CNN
F 2 "Inductor_SMD:L_Bourns_SRF1260" H 2300 2450 50  0001 C CNN
F 3 "~" H 2300 2450 50  0001 C CNN
	1    2300 2450
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 5FAEB97A
P 2250 1900
F 0 "J1" V 2250 1650 50  0000 R CNN
F 1 "Conn_01x04" V 2123 1612 50  0001 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 2250 1900 50  0001 C CNN
F 3 "~" H 2250 1900 50  0001 C CNN
	1    2250 1900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2100 2350 2100 2150
Wire Wire Line
	2100 2150 2150 2150
Wire Wire Line
	2150 2150 2150 2100
Wire Wire Line
	2450 2100 2450 2200
Wire Wire Line
	2450 2200 2050 2200
Wire Wire Line
	2050 2200 2050 2550
Wire Wire Line
	2050 2550 2100 2550
Wire Wire Line
	2500 2350 2500 2250
Wire Wire Line
	2500 2250 2350 2250
Wire Wire Line
	2350 2250 2350 2100
Wire Wire Line
	2250 2100 2250 2150
Wire Wire Line
	2250 2150 2550 2150
Wire Wire Line
	2550 2150 2550 2550
Wire Wire Line
	2550 2550 2500 2550
$EndSCHEMATC
