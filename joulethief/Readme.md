Joule Thief
===========

This is a direct implementation of a standard Joule Thief voltage booster
design coupled with a feedback mechanism and a linear voltage regulator.
Normally this sort of experimental project I would build out on prototyping
board but all the off-the-shelf coupled inductors I could find from my usual
supplier were surface-mount rather than through-hole.

Note that the circuit was made using KiCad 5.1.6 and uses some components
that are not included in the KiCad 5.1.4 symbol library.


## Circuit schematic

![Schematic](schematic.png)

## Licencing
See the [top-level Readme.md](../Readme.md) for licencing details.


## Contact

`remy.horton` `(at)` `gmail.com`
