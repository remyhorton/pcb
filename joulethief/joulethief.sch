EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Joule Thief PSU"
Date "30 Sept. 2020"
Rev "Rev.1"
Comp "Remy Horton"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:L_Core_Ferrite_Coupled_1324 L1
U 1 1 5F74F77A
P 7500 2700
F 0 "L1" H 7500 2467 50  0000 C CNN
F 1 "L_Core_Ferrite_Coupled_1324" H 7500 2466 50  0001 C CNN
F 2 "Inductor_SMD:L_Bourns_SRF1260" H 7500 2700 50  0001 C CNN
F 3 "~" H 7500 2700 50  0001 C CNN
	1    7500 2700
	-1   0    0    1   
$EndComp
$Comp
L Device:R R1
U 1 1 5F75168A
P 7150 3050
F 0 "R1" H 7220 3096 50  0000 L CNN
F 1 "1k" H 7220 3005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7080 3050 50  0001 C CNN
F 3 "~" H 7150 3050 50  0001 C CNN
	1    7150 3050
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BC547 Q1
U 1 1 5F75F495
P 7700 3200
F 0 "Q1" H 7890 3200 50  0000 L CNN
F 1 "BC547" H 7891 3155 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 7900 3125 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC547.pdf" H 7700 3200 50  0001 L CNN
	1    7700 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 2800 7800 2800
Wire Wire Line
	7800 2800 7800 3000
Wire Wire Line
	7700 2600 7700 2500
Wire Wire Line
	7500 3200 7450 3200
$Comp
L Diode:1N5819 D1
U 1 1 5F76E368
P 7950 3000
F 0 "D1" H 7950 2875 50  0000 C CNN
F 1 "1N5819" H 7950 2874 50  0001 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 7950 2825 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88525/1n5817.pdf" H 7950 3000 50  0001 C CNN
	1    7950 3000
	-1   0    0    1   
$EndComp
Connection ~ 7800 3000
$Comp
L Device:D_Zener D2
U 1 1 5F771C1C
P 8100 3150
F 0 "D2" H 8100 3250 50  0000 C CNN
F 1 "D_Zener" H 8100 3024 50  0001 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P12.70mm_Horizontal" H 8100 3150 50  0001 C CNN
F 3 "~" H 8100 3150 50  0001 C CNN
	1    8100 3150
	0    1    1    0   
$EndComp
$Comp
L Transistor_BJT:BC547 Q2
U 1 1 5F772269
P 7350 3400
F 0 "Q2" H 7540 3400 50  0000 L CNN
F 1 "BC547" H 7541 3355 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 7550 3325 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC547.pdf" H 7350 3400 50  0001 L CNN
	1    7350 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5F77A676
P 8000 3450
F 0 "R2" H 8070 3496 50  0000 L CNN
F 1 "1k5" H 8070 3405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7930 3450 50  0001 C CNN
F 3 "~" H 8000 3450 50  0001 C CNN
	1    8000 3450
	1    0    0    -1  
$EndComp
Connection ~ 7450 3200
Wire Wire Line
	7450 3200 7150 3200
Wire Wire Line
	7000 3200 7000 3600
Connection ~ 7450 3600
Wire Wire Line
	7150 3650 7150 3400
Wire Wire Line
	7800 3400 7800 3600
Connection ~ 7800 3600
Wire Wire Line
	7800 3600 7450 3600
Wire Wire Line
	7150 3650 7950 3650
$Comp
L Regulator_Linear:uA7805 U1
U 1 1 5F788AC8
P 8650 3000
F 0 "U1" H 8650 3242 50  0000 C CNN
F 1 "uA7805" H 8650 3151 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 8675 2850 50  0001 L CIN
F 3 "http://www.ti.com/lit/ds/symlink/ua78.pdf" H 8650 2950 50  0001 C CNN
	1    8650 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5F789622
P 8350 3300
F 0 "C1" H 8465 3346 50  0000 L CNN
F 1 "220u" H 8400 3200 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 8388 3150 50  0001 C CNN
F 3 "~" H 8350 3300 50  0001 C CNN
	1    8350 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5F789D9E
P 8950 3300
F 0 "C2" H 8750 3350 50  0000 L CNN
F 1 "220u" H 8700 3200 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 8988 3150 50  0001 C CNN
F 3 "~" H 8950 3300 50  0001 C CNN
	1    8950 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 3450 8350 3600
Wire Wire Line
	8350 3600 8650 3600
Wire Wire Line
	8650 3600 8650 3300
Wire Wire Line
	8650 3600 8950 3600
Wire Wire Line
	8950 3600 8950 3450
Connection ~ 8650 3600
Wire Wire Line
	8100 3300 8000 3300
Wire Wire Line
	7950 3300 7950 3650
Wire Wire Line
	8350 3150 8350 3000
Wire Wire Line
	8950 3150 8950 3000
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5F79B55A
P 8250 2700
F 0 "J1" V 8350 2700 50  0000 R CNN
F 1 "Conn_01x02" V 8213 2780 50  0001 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 8250 2700 50  0001 C CNN
F 3 "~" H 8250 2700 50  0001 C CNN
	1    8250 2700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8350 3000 8100 3000
Connection ~ 8350 3000
Connection ~ 8100 3000
Wire Wire Line
	8250 2900 8250 3600
Wire Wire Line
	8350 2900 8350 3000
Wire Wire Line
	8350 3600 8250 3600
Connection ~ 8350 3600
Connection ~ 8250 3600
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5F7AEDE9
P 8100 2700
F 0 "#FLG0101" H 8100 2775 50  0001 C CNN
F 1 "PWR_FLAG" H 8100 2873 50  0000 C CNN
F 2 "" H 8100 2700 50  0001 C CNN
F 3 "~" H 8100 2700 50  0001 C CNN
	1    8100 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 2900 8100 2900
Wire Wire Line
	8100 2900 8100 2700
Connection ~ 8250 2900
Connection ~ 8000 3300
Wire Wire Line
	8000 3300 7950 3300
Connection ~ 8000 3600
Wire Wire Line
	8000 3600 7800 3600
Wire Wire Line
	8000 3600 8250 3600
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 5F7B4C31
P 9200 3350
F 0 "J2" V 9300 3350 50  0000 R CNN
F 1 "Conn_01x02" V 9163 3430 50  0001 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 9200 3350 50  0001 C CNN
F 3 "~" H 9200 3350 50  0001 C CNN
	1    9200 3350
	-1   0    0    1   
$EndComp
Wire Wire Line
	8950 3000 9400 3000
Wire Wire Line
	9400 3000 9400 3250
Connection ~ 8950 3000
Wire Wire Line
	9400 3350 9400 3600
Connection ~ 8950 3600
Connection ~ 8350 2900
Wire Wire Line
	8500 2900 8350 2900
Wire Wire Line
	8500 2700 8500 2900
Wire Wire Line
	9400 3600 8950 3600
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5F7AF366
P 8500 2700
F 0 "#FLG0102" H 8500 2775 50  0001 C CNN
F 1 "PWR_FLAG" H 8500 2873 50  0000 C CNN
F 2 "" H 8500 2700 50  0001 C CNN
F 3 "~" H 8500 2700 50  0001 C CNN
	1    8500 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3600 7000 3600
$Comp
L Device:Battery_Cell BT1
U 1 1 5F767111
P 7000 3100
F 0 "BT1" H 6750 3200 50  0000 L CNN
F 1 "Battery_Cell" H 6450 3100 50  0001 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" V 7000 3160 50  0001 C CNN
F 3 "~" V 7000 3160 50  0001 C CNN
	1    7000 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 2500 7000 2500
Wire Wire Line
	7000 2500 7000 2800
Wire Wire Line
	7300 2600 7150 2600
Wire Wire Line
	7150 2600 7150 2900
Wire Wire Line
	7300 2800 7000 2800
Connection ~ 7000 2800
Wire Wire Line
	7000 2800 7000 2900
$EndSCHEMATC
