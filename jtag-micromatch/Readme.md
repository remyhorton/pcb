JTAG MicroMaTch programming adapter
===================================
This PCB is an adapter that provides a MicroMaTch-based electrical interface for JTAG programming connections. The full background is [detailed elsewhere][remy-article] but in summary the motive is due to the cost difference between MicroMaTch connectors and the 10-pin mini-ITC connector normally used for flashing firmware to ARM-based microcontrollers.


## Pin-out
`IDC` refers to the 20-pin IDC interface that connects to the programmer, whereas `10-pin` and `6-pin` are respectively the 10-pin and 6-pin MicroMaTch interface that is supposed to connect to the microcontroller host board.

| Pin name       | IDC | 10-pin | 6-pin
| --------:      | :-: | :----: | :---:
| Vcc sense      | 1,2 | 1      | 1
| `TRST` (_n/c_) | 3   | -      | -
| Ground         | 4   | 3,5,9  | 5
| `TDI`          | 5   | 8      | -
| `SWDIO/TMS`    | 7   | 2      | 2
| `SWCLK/TCLK`   | 9   | 4      | 4
| `RCLK`         | 11  | -      | -
| `SWO/TDO`      | 13  | 6      | -
| Reset          | 15  | 10     | 6
| _n/c_          | 17  | -      | -
| _n/c_          | 19  | -      | -


Note that `RTLK` is shown as `RTCK` in some guides; IDC pins 4, 6, 8, 10, 12, 14, 16, 18, and 20 are connected to ground; and pin 3 of the 6-pin and pin 7 of the 10-pin MicroMaTch connectors are both not connected.


## Components

| Component                         | Manufacturer    | Part number
| :-------------------------------: | :----------:    | :---------:
| 20-pin right-angled polarised IDC | Valcon          | `B07a-20-AGA1-G`
| 6-pin surface-mount MicroMaTch    | TE Connectivity | `2178711-6`
| 10-pin surface-mount MicroMaTch   | TE Connectivity | `1-2178711-0`

I am not sure why the 10-pin MicroMaTch splits the ten within the part number between the start and end.


## Licencing
See the [top-level Readme.md](../Readme.md) for licencing details.


## Contact

`remy.horton` `(at)` `gmail.com`


[remy-article]: https://www.remy.org.uk/elec.php?elec=1586646000


