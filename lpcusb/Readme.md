NXP LPC11Uxx USB mini-evaluation board
======================================
This is a small break-out board for the NXP `LPC11Uxx` which is intended as a simple platform for the development of USB firmware.
I have released it publicly because I am not aware of any off-the-shelf evaluation board that is still in production and it is hard enough getting the firmware working without having to worry whether the hardware itself has its own problems.


## Circuit schematic

The schematic is in [lpcusb.pdf](lpcusb.pdf).
My website article has a [detailed discussion][circuit-article] of the individual sections of the circuit.


It is unclear whether the `LPC11Uxx` actually supports low-speed (i.e. 1.5MHz) USB or not.
All versions of reference manual `UM10462` I have been able to obtain repeatedly mention low-speed USB, yet there are also official indications that it [only supports full-speed][nxp-forum-post] USB.
Even though so far I have been unable to get low-speed to show the same signs of life as full-speed I opted to leave the speed selector pins in place.


## Components

| Pad   | Component              | Manufacturer        | Part number
| :---: | :--------------------: | :-----------------: | :----------:
| U1    | Microcontroller        | NXP                 | `LPC11U12FBD48/201`
| X1 	| 16MHz oscillator       | Multicomp           | `MCSJK‑6NC2‑16.00‑5‑B`
| R1,R2 | 33Ω resisitor          | Multicomp           | `MCWR08X33R0FTL`
| R3    | 24kΩ resisitor         | Multicomp           | `MCWR08X2402FTL`
| R4    | 39kΩ resisitor         | Multicomp           | `MCWR08X3902FTL`
| R5    | 1.5kΩ resisitor        | Multicomp           | `MCWR08X1501FTL`
| C1,C2 | 0.1μF capacitor        | Multicomp           | `MC0805B104K250CT`
| C3,C4 | 100pF SMD capacitor    | Kemet               | `C0805C101JAGACTU`
| Q1    | 10k/47k PNP transistor | ON Semiconductor    | `MMUN2114LT1G`
| J2    | USB Mini Type-B        | Wurth Elektronik    | `65100516121`
| J4    | 6-pin receptacle       | AMP/TE Connectivity | `2178711-6`
| n/a   | Jumper                 | Multicomp           | `MC-2228CG`


## Licencing
See the [top-level Readme.md](../Readme.md) for licencing details.


## Contact

`remy.horton` `(at)` `gmail.com`


[circuit-article]: https://www.remy.org.uk/elec.php?elec=1602284400#design
[nxp-forum-post]: https://community.nxp.com/t5/LPCXpresso-IDE/low-speed-USB/m-p/597062/highlight/true#M32125
