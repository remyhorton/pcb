Micromatch 16-pin breakout
==========================
A breakout board that allows a 16-pin TE Connectivity MicroMaTch connector to be plugged into solderless prototyping board.
Breaking out is either dual-inline using through-hole header pins, or single-inline using surface-mount header pins.

![Rendering](rendering.png)


## Components

 Pad num  | Component             | Manufacturer    | Part number
 :------: | :-------------------: | :-------------: | :---------:
 `J1`     | MicroMatch 16-pin SMD | TE Conenctivity | `1-2178711-6`
 `J2`     | 8-pin header (THT)    | Wurth           | `61300811121`
 `J3,J4`  | 8-pin header (SMD)    | Samtec          | `TSM-116-03-L-SV`


## Distribution

The main distribution point for this design is [https://bitbucket.org/remyhorton/pcb/src/master/micromatch16-breakout/][bitbucket].


## Licencing
This PCB desiogn is covered by v1.2 of the [CERN Open Hardware Licence][cern-ohl].
In short you are free to have copies of these PCBs fabricated but if you distribute them (especially for profit) you must provide a link back to the [distribution repository][bitbucket].
In addition by distributing any fabricated PCBs you accept any and all legal responsibilities related to product safety and fitness for purpose.


## Contact

`remy.horton` `(at)` `gmail.com`

[cern-ohl]: https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-1.2
[bitbucket]: https://bitbucket.org/remyhorton/pcb/src/master/micromatch16-breakout/