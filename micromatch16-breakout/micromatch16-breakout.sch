EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "MicroMaTCh 16-pin breakout"
Date "15 November 2020"
Rev "Rev.1"
Comp "Remy Horton"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x16 J2
U 1 1 5FB13D5C
P 1650 1950
F 0 "J2" V 1650 2750 50  0000 C CNN
F 1 "Conn_01x16" H 1730 1851 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x16_P2.54mm_Vertical_SMD_Pin1Right" H 1650 1950 50  0001 C CNN
F 3 "~" H 1650 1950 50  0001 C CNN
	1    1650 1950
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J3
U 1 1 5FB14351
P 1250 950
F 0 "J3" V 1250 1400 50  0000 R CNN
F 1 "Conn_01x08" H 1330 851 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 1250 950 50  0001 C CNN
F 3 "~" H 1250 950 50  0001 C CNN
	1    1250 950 
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J4
U 1 1 5FB165CC
P 2200 950
F 0 "J4" V 2200 1400 50  0000 R CNN
F 1 "Conn_01x08" H 2280 851 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 2200 950 50  0001 C CNN
F 3 "~" H 2200 950 50  0001 C CNN
	1    2200 950 
	0    -1   -1   0   
$EndComp
Text GLabel 2450 2150 3    50   Input ~ 0
16
Text GLabel 2350 2150 3    50   Input ~ 0
15
Text GLabel 2250 2150 3    50   Input ~ 0
14
Text GLabel 2150 2150 3    50   Input ~ 0
13
Text GLabel 2050 2150 3    50   Input ~ 0
12
Text GLabel 1950 2150 3    50   Input ~ 0
11
Text GLabel 1850 2150 3    50   Input ~ 0
10
Text GLabel 1750 2150 3    50   Input ~ 0
9
Text GLabel 1650 2150 3    50   Input ~ 0
8
Text GLabel 1550 2150 3    50   Input ~ 0
7
Text GLabel 1450 2150 3    50   Input ~ 0
6
Text GLabel 1350 2150 3    50   Input ~ 0
5
Text GLabel 1250 2150 3    50   Input ~ 0
4
Text GLabel 1150 2150 3    50   Input ~ 0
3
Text GLabel 1050 2150 3    50   Input ~ 0
2
Text GLabel 950  2150 3    50   Input ~ 0
1
$Comp
L Connector_Generic:Conn_01x16 J1
U 1 1 5FB18DBB
P 1650 1450
F 0 "J1" V 1650 2250 50  0000 C CNN
F 1 "Conn_01x16" H 1730 1351 50  0001 L CNN
F 2 "stm32l4r5:ConnectorMicromatch-16" H 1650 1450 50  0001 C CNN
F 3 "~" H 1650 1450 50  0001 C CNN
	1    1650 1450
	0    -1   -1   0   
$EndComp
Text GLabel 2450 1650 3    50   Input ~ 0
16
Text GLabel 2350 1650 3    50   Input ~ 0
15
Text GLabel 2250 1650 3    50   Input ~ 0
14
Text GLabel 2150 1650 3    50   Input ~ 0
13
Text GLabel 2050 1650 3    50   Input ~ 0
12
Text GLabel 1950 1650 3    50   Input ~ 0
11
Text GLabel 1850 1650 3    50   Input ~ 0
10
Text GLabel 1750 1650 3    50   Input ~ 0
9
Text GLabel 1650 1650 3    50   Input ~ 0
8
Text GLabel 1550 1650 3    50   Input ~ 0
7
Text GLabel 1450 1650 3    50   Input ~ 0
6
Text GLabel 1350 1650 3    50   Input ~ 0
5
Text GLabel 1250 1650 3    50   Input ~ 0
4
Text GLabel 1150 1650 3    50   Input ~ 0
3
Text GLabel 1050 1650 3    50   Input ~ 0
2
Text GLabel 950  1650 3    50   Input ~ 0
1
Text GLabel 2600 1150 3    50   Input ~ 0
16
Text GLabel 2500 1150 3    50   Input ~ 0
15
Text GLabel 2400 1150 3    50   Input ~ 0
14
Text GLabel 2300 1150 3    50   Input ~ 0
13
Text GLabel 2200 1150 3    50   Input ~ 0
12
Text GLabel 2100 1150 3    50   Input ~ 0
11
Text GLabel 2000 1150 3    50   Input ~ 0
10
Text GLabel 1900 1150 3    50   Input ~ 0
9
Text GLabel 1650 1150 3    50   Input ~ 0
8
Text GLabel 1550 1150 3    50   Input ~ 0
7
Text GLabel 1450 1150 3    50   Input ~ 0
6
Text GLabel 1350 1150 3    50   Input ~ 0
5
Text GLabel 1250 1150 3    50   Input ~ 0
4
Text GLabel 1150 1150 3    50   Input ~ 0
3
Text GLabel 1050 1150 3    50   Input ~ 0
2
Text GLabel 950  1150 3    50   Input ~ 0
1
$EndSCHEMATC
