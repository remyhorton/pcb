PIC16F1454 USB mini-evaluation board
====================================
This PCB is a host board for the Microchip  `PIC16F1454` which is intended for the development of USB firmware, the latter of which is covered in a [website article][article] that discusses this mini-evaluation board in more depth.
A jumper is provided that allows the chip to be powered either from the USB bus or via an external power jack.
For convenience there is a header for  TTL-level RS232 which provides a side-channel for debugging, as well as a flash header for in-circuit serial programming.


## Circuit schematic

![Schematic](schematic1.png)
![Schematic](schematic2.png)

## Components

 Pad num  | Component                 | Manufacturer      | Part number
 :------: | :-----------------------: | :---------------: | :---------:
 `U1`     | 5v linear regulator       | ON Semiconductor  | `MC7805ACTG`
 `U2`     | Microcontroller           | Microchip         | `PIC16F1454`
 `Y1`     | 32.868kHz crystal         | Abracon           | `AB38T-32.768KHZ`
 `D1`     | Through-hole Red LED      | Kingbright        | `L-113IDT`
 `J1`     | Barrel jack               | CUI Devices       | `PJ-102AH`
 `JP1, J2`| Pin header strip          | Multicomp         | `2211S-22G`
 `J5`     | 6-way receptacle          | Multicomp         | `2212S-06SG-85`
 `J6`     | USB Mini-B connector      | Wurth             | `65100516121`
 `R3`     | Protective resisitor      | Multicomp         | `MCWR08X1001FTL`
 `R4`     | 200k? resistor            | Multicomp         | `MCWR08X2000FTL`
 `R5`     | 100k? resistor            | Multicomp         | `MC01W08055100K`
 `C1`     | 0.47μF ceramic capacitor | Multicomp         | `MCB0805R474KCT`
 `C2`     | 0.01μF ceramic capacitor | Samsung           | `CL21B103KCANNNC`
 `C3`     | 0.1μF ceramic capacitor  | Samsung           | `CL21B104KACNNNC`
 `C4, C5` | 22pF capacitors           | Vishay            | `VJ0805A220FXBPW1BC`


## Licencing
See the [top-level Readme.md](../Readme.md) for licencing details.


## Contact

`remy.horton` `(at)` `gmail.com`

[article]: http://www.remy.org.uk/elec.php?elec=unpublished