Power Jack mini-PCB
===================
This is a simple power adapter for providing a constant voltage to a circuit that does not carry its own linear regulator. It was originally intended for convenience when using prototyping board, but due to the pricing model of some fabrication services there were significant savings to be made by using an external regulator rather than using up real-estate for an on-board one.

![Schematic](picture.jpeg)


## Circuit schematic and componants

![Schematic](schematic.png)


## Components

 PCB#  | Component                | Manufacturer      | Part number
 :---: | :----------------------: | :---------------: | :---------:
 `U1`  | Microcontroller          | Microchip         | `PIC12F1822-I/P`
 `J1`  | 6-pin receptacle         | Multicomp         | `2212S-06SG-85`
 `J2`  | Barrel jack              | Cliff Electronics | `FC681465P`

## Licencing
See the [top-level Readme.md](../Readme.md) for licencing details.


## Contact

`remy.horton` `(at)` `gmail.com`
