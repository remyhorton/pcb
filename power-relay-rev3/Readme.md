Power relay circuit (Rev.3)
===========================

I2C-controlled DPDT power relay switch.
The double-sided tracks for the relay  connections should be able to carry several amperes.

## Circuit schematic

![Schematic](schematic.png)

## Components

 Pad num  | Component                | Manufacturer      | Part number
 :------: | :----------------------: | :---------------: | :---------:
 `K1`     | Relay switch             | Shrack - TE Conn. | `RT424005`
 `U1`     | 4-bit I/O Expander       | NXP               | `9D2025`
 `U2`     | 5v linear regulator      | ON Semiconductor  | `MC7805ACTG`
`R1,R2,R3`| 10kOhm resistor          | Multicomp         | `MF12 10K`
 `Q1`     | NPN transistor           | ON Semiconductor  | `BC547BTF`
 `J1,J2`  | 3-way terminal block     | Phoenix Contact   | `MKDS 1,5/3`
 `J3`     | 3-pin receptacle         | Multicomp         | `2212S-03SG-85`
 `J4`     | Barrel jack              | CUI Devices       | `PJ-102AH`
 `J5`     | 2-Pin receptacle         | Multicomp         | `2212S-02SG-85`


## Licencing
See the [top-level Readme.md](../Readme.md) for licencing details.

If you use this circuit to switch high-voltage electrical power you do so at your own risk.


## Contact

`remy.horton` `(at)` `gmail.com`
