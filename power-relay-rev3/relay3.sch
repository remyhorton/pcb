EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Power Relay"
Date "18 October 2020"
Rev "Rev.3"
Comp "Remy Horton"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Relay:RT42xAxx K1
U 1 1 5F8B6E89
P 4500 2850
F 0 "K1" V 5267 2850 50  0000 C CNN
F 1 "RT42xAxx" V 5176 2850 50  0000 C CNN
F 2 "Relay_THT:Relay_DPDT_Schrack-RT2-FormC_RM5mm" H 4500 2850 50  0001 C CNN
F 3 "http://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Data+Sheet%7FRT2_bistable%7F1116%7Fpdf%7FEnglish%7FENG_DS_RT2_bistable_1116.pdf%7F1-1415537-8" H 4500 2850 50  0001 C CNN
	1    4500 2850
	0    -1   -1   0   
$EndComp
Text GLabel 5250 2950 0    50   Input ~ 0
ActiveON
$Comp
L Connector_Generic:Conn_01x03 J1
U 1 1 5F8B8767
P 5450 2450
F 0 "J1" H 5400 2650 50  0000 L CNN
F 1 "Conn_01x03" V 5323 2262 50  0001 R CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-3-5.08_1x03_P5.08mm_Horizontal" H 5450 2450 50  0001 C CNN
F 3 "~" H 5450 2450 50  0001 C CNN
	1    5450 2450
	1    0    0    -1  
$EndComp
Text GLabel 5250 2550 0    50   Input ~ 0
NeutralOFF
Text GLabel 5250 2350 0    50   Input ~ 0
NeutralON
Text GLabel 4200 2750 0    50   Input ~ 0
ActiveON
Text GLabel 4200 2550 0    50   Input ~ 0
NeutralOFF
Text GLabel 4200 2350 0    50   Input ~ 0
NeutralON
Text GLabel 4200 2950 0    50   Input ~ 0
ActiveOFF
Wire Wire Line
	5250 2450 4800 2450
$Comp
L Transistor_BJT:BC547 Q1
U 1 1 5F8B84A4
P 4950 3450
F 0 "Q1" H 5141 3496 50  0000 L CNN
F 1 "BC547" H 5141 3405 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 5150 3375 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC547.pdf" H 4950 3450 50  0001 L CNN
	1    4950 3450
	1    0    0    -1  
$EndComp
$Comp
L Interface_Expansion:PCA9536D U1
U 1 1 5F8B9220
P 3800 3700
F 0 "U1" V 4150 4000 50  0000 R CNN
F 1 "PCA9536D" V 3350 4050 50  0000 R CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4800 3350 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/PCA9536.pdf" H 3600 2000 50  0001 C CNN
	1    3800 3700
	0    -1   -1   0   
$EndComp
$Comp
L Regulator_Linear:uA7805 U2
U 1 1 5F8BCBF4
P 5550 3850
F 0 "U2" H 5550 4092 50  0000 C CNN
F 1 "uA7805" H 5550 4001 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 5575 3700 50  0001 L CIN
F 3 "http://www.ti.com/lit/ds/symlink/ua78.pdf" H 5550 3800 50  0001 C CNN
	1    5550 3850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Barrel_Jack_MountingPin J4
U 1 1 5F8BD456
P 4950 3950
F 0 "J4" H 4800 4150 50  0000 C CNN
F 1 "Barrel_Jack_MountingPin" H 5007 4176 50  0001 C CNN
F 2 "Connector_BarrelJack:BarrelJack_CUI_PJ-102AH_Horizontal" H 5000 3910 50  0001 C CNN
F 3 "~" H 5000 3910 50  0001 C CNN
	1    4950 3950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5F8C090F
P 4300 3750
F 0 "#PWR03" H 4300 3500 50  0001 C CNN
F 1 "GND" H 4305 3577 50  0000 C CNN
F 2 "" H 4300 3750 50  0001 C CNN
F 3 "" H 4300 3750 50  0001 C CNN
	1    4300 3750
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR01
U 1 1 5F8C0F71
P 3500 3700
F 0 "#PWR01" H 3500 3550 50  0001 C CNN
F 1 "VCC" H 3515 3873 50  0000 C CNN
F 2 "" H 3500 3700 50  0001 C CNN
F 3 "" H 3500 3700 50  0001 C CNN
	1    3500 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5F8C1440
P 4450 3550
F 0 "R1" V 4350 3450 50  0000 C CNN
F 1 "10k" V 4350 3650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" V 4380 3550 50  0001 C CNN
F 3 "~" H 4450 3550 50  0001 C CNN
	1    4450 3550
	0    -1   -1   0   
$EndComp
$Comp
L power:VCC #PWR02
U 1 1 5F8C3845
P 4200 3250
F 0 "#PWR02" H 4200 3100 50  0001 C CNN
F 1 "VCC" H 4215 3423 50  0000 C CNN
F 2 "" H 4200 3250 50  0001 C CNN
F 3 "" H 4200 3250 50  0001 C CNN
	1    4200 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 3550 4150 3550
Wire Wire Line
	4150 3550 4150 3150
Wire Wire Line
	4150 3150 3700 3150
Wire Wire Line
	3700 3150 3700 3200
NoConn ~ 3800 3200
NoConn ~ 3900 3200
NoConn ~ 4000 3200
$Comp
L Connector_Generic:Conn_01x03 J3
U 1 1 5F8C89F1
P 4450 4050
F 0 "J3" V 4550 4000 50  0000 R CNN
F 1 "Conn_01x03" H 4530 4001 50  0001 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 4450 4050 50  0001 C CNN
F 3 "~" H 4450 4050 50  0001 C CNN
	1    4450 4050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4350 4250 4200 4250
Wire Wire Line
	4200 3700 4200 4250
Wire Wire Line
	4450 4250 4450 4300
Wire Wire Line
	4450 4300 3900 4300
Wire Wire Line
	3900 4300 3900 4200
Wire Wire Line
	4550 4250 4550 4350
Wire Wire Line
	4550 4350 4000 4350
Wire Wire Line
	4000 4350 4000 4200
Wire Wire Line
	4200 3700 4300 3700
Wire Wire Line
	4300 3700 4300 3750
Connection ~ 4200 3700
Wire Wire Line
	5250 4050 5250 4150
Wire Wire Line
	5250 4150 5400 4150
$Comp
L power:GND #PWR04
U 1 1 5F8CF1BB
P 5400 4150
F 0 "#PWR04" H 5400 3900 50  0001 C CNN
F 1 "GND" H 5405 3977 50  0000 C CNN
F 2 "" H 5400 4150 50  0001 C CNN
F 3 "" H 5400 4150 50  0001 C CNN
	1    5400 4150
	1    0    0    -1  
$EndComp
Connection ~ 5400 4150
Wire Wire Line
	5400 4150 5550 4150
$Comp
L Device:R R3
U 1 1 5F8CFE33
P 5650 3200
F 0 "R3" H 5720 3246 50  0000 L CNN
F 1 "10k" H 5720 3155 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" V 5580 3200 50  0001 C CNN
F 3 "~" H 5650 3200 50  0001 C CNN
	1    5650 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5F8D0825
P 5550 3200
F 0 "R2" H 5400 3250 50  0000 L CNN
F 1 "10k" H 5350 3150 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" V 5480 3200 50  0001 C CNN
F 3 "~" H 5550 3200 50  0001 C CNN
	1    5550 3200
	1    0    0    -1  
$EndComp
Text GLabel 5550 3350 3    50   Input ~ 0
SDA
Text GLabel 5650 3350 3    50   Input ~ 0
SCL
Wire Wire Line
	4600 3550 4750 3550
Wire Wire Line
	4750 3550 4750 3450
Wire Wire Line
	4800 3250 5050 3250
Wire Wire Line
	4300 3700 4900 3700
Wire Wire Line
	5050 3700 5050 3650
Connection ~ 4300 3700
NoConn ~ 4950 4250
$Comp
L Connector_Generic:Conn_01x02 J5
U 1 1 5F8D836D
P 5600 2550
F 0 "J5" H 5600 2350 50  0000 C CNN
F 1 "Conn_01x02" H 5680 2451 50  0001 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 5600 2550 50  0001 C CNN
F 3 "~" H 5600 2550 50  0001 C CNN
	1    5600 2550
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR08
U 1 1 5F8D87E2
P 5850 3850
F 0 "#PWR08" H 5850 3700 50  0001 C CNN
F 1 "VCC" H 5865 4023 50  0000 C CNN
F 2 "" H 5850 3850 50  0001 C CNN
F 3 "" H 5850 3850 50  0001 C CNN
	1    5850 3850
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR06
U 1 1 5F8DA42F
P 5800 2450
F 0 "#PWR06" H 5800 2300 50  0001 C CNN
F 1 "VCC" H 5815 2623 50  0000 C CNN
F 2 "" H 5800 2450 50  0001 C CNN
F 3 "" H 5800 2450 50  0001 C CNN
	1    5800 2450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5F8DA7C6
P 5800 2550
F 0 "#PWR07" H 5800 2300 50  0001 C CNN
F 1 "GND" H 5805 2377 50  0000 C CNN
F 2 "" H 5800 2550 50  0001 C CNN
F 3 "" H 5800 2550 50  0001 C CNN
	1    5800 2550
	1    0    0    -1  
$EndComp
Text GLabel 3900 4300 0    50   Input ~ 0
SDA
Text GLabel 4550 4300 2    50   Input ~ 0
SCL
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5F8DB228
P 5250 3850
F 0 "#FLG0101" H 5250 3925 50  0001 C CNN
F 1 "PWR_FLAG" H 5250 4023 50  0001 C CNN
F 2 "" H 5250 3850 50  0001 C CNN
F 3 "~" H 5250 3850 50  0001 C CNN
	1    5250 3850
	1    0    0    -1  
$EndComp
Connection ~ 5250 3850
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5F8DB711
P 4900 3700
F 0 "#FLG0102" H 4900 3775 50  0001 C CNN
F 1 "PWR_FLAG" H 4900 3873 50  0001 C CNN
F 2 "" H 4900 3700 50  0001 C CNN
F 3 "~" H 4900 3700 50  0001 C CNN
	1    4900 3700
	1    0    0    -1  
$EndComp
Connection ~ 4900 3700
Wire Wire Line
	4900 3700 5050 3700
Connection ~ 5600 3050
Wire Wire Line
	5550 3050 5600 3050
Wire Wire Line
	5650 3050 5600 3050
Wire Wire Line
	4800 2850 5250 2850
$Comp
L power:VCC #PWR05
U 1 1 5F8D12AE
P 5600 3050
F 0 "#PWR05" H 5600 2900 50  0001 C CNN
F 1 "VCC" H 5615 3223 50  0000 C CNN
F 2 "" H 5600 3050 50  0001 C CNN
F 3 "" H 5600 3050 50  0001 C CNN
	1    5600 3050
	1    0    0    -1  
$EndComp
Text GLabel 5250 2750 0    50   Input ~ 0
ActiveOFF
$Comp
L Connector_Generic:Conn_01x03 J2
U 1 1 5F8B7BBF
P 5450 2850
F 0 "J2" H 5500 2650 50  0000 R CNN
F 1 "Conn_01x03" V 5323 2662 50  0001 R CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-3-5.08_1x03_P5.08mm_Horizontal" H 5450 2850 50  0001 C CNN
F 3 "~" H 5450 2850 50  0001 C CNN
	1    5450 2850
	1    0    0    -1  
$EndComp
$EndSCHEMATC
