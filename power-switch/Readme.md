Power Relay circuit, Rev.2
==========================
Second revision of my [previous power relay](../relay).
This version uses a simple high-low signal rather than using RS232 and takes its power for the circuitry from the AC mains.
The design was abandoned before it was sent for fabrication so it has never been tested.


## Circuit schematic

![Schematic](schematic.png)


## Licencing
See the [top-level Readme.md](../Readme.md) for licencing details.

If you use this circuit to switch high-voltage electrical power you do so at your own risk.


## Contact

`remy.horton` `(at)` `gmail.com`
