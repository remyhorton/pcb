14-pin PIC programming seat
===========================
This is a programming seat for flashing firmware to 14-pin Microchip PIC which also includes an on-board power supply.
In practice flashing PIC Microcontrollers always needs some form of external power supply and these days I only use 14-pin chips such as the PIC1454 and PIC16F1823, so this is an all-in-one unit that is intended specifically for these parts.
Gerber files suited for fabrication by JLCPCB are provided but they should be suited for most if not all other PCB services.

![Schematic](picture.jpeg)


## Licencing
This PCB design is covered by v1.2 of the [CERN Open Hardware Licence][cern-ohl].
In short you are free to have copies of these PCBs fabricated but if you distribute them (especially for profit) you must provide a link back to the [distribution repository][bitbucket].
In addition by distributing any fabricated PCBs you accept any and all legal responsibilities related to product safety and fitness for purpose.


## Distribution

The main distribution point for this design is [https://bitbucket.org/remyhorton/pcb/src/master/programming-seat/][bitbucket].


## Contact

`remy.horton` `(at)` `gmail.com`


[cern-ohl]: https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-1.2
[bitbucket]: https://bitbucket.org/remyhorton/pcb/src/master/programming-seat/