Power Relay circuit
===================
RS232-controlled power relay switch.


## Circuit schematic

![Schematic](schematic.png)


## Components

 PCB#  | Component                | Manufacturer      | Part number
 :---: | :----------------------: | :---------------: | :---------:
 `U2`  | Microcontroller          | Microchip         | `PIC12F1822-I/P`
 `U1`  | 5v linear regulator      | ON Semiconductor  | `MC7805ACTG`
 `Q1`  | PNP transistor           | ON Semiconductor  | `BC556BTF`
 `K1`  | Relay switch             | Kemet             | `EC2-5NU`
 `R1`  | 10kOhm resistor          | Multicomp         | `MF12 10K`
 `R2`  |  2kOhm resistor          | Multicomp         | `MF12 2K`
 `J1`  | 6-pin receptacle         | Multicomp         | `2212S-06SG-85`
 `J2`  | Barrel jack              | Cliff Electronics | `FC681465P`
`J3,J4`| Terminal block           | Phoenix Contact   | `MKDS 1,5/3`
 `J5`  | Pin header strip         | Harwin            | `M20-9991045`

## Licencing
See the [top-level Readme.md](../Readme.md) for licencing details.

If you use this circuit to switch high-voltage electrical power you do so at your own risk.


## Contact

`remy.horton` `(at)` `gmail.com`


[remy-article]: https://www.remy.org.uk/elec.php?elec=1596150000
