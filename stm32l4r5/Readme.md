STM32L4 Cortex-M4 mini-evaluation board
=======================================
This is a mini development board based around the _ST Microelectronics_ `STM32L4R5`
 Cortex-M4 microcontroller.
It was originally designed as a training aid in order to get experience using the microcontroller, having interfaces for General Purpose I/O, TTL-level RS232 and I2C serial communications, JTAG/SWD, and USB.
The circuit design is derived from the reference given in technical note [AN4555: Getting started with STM32L4 Series and STM32L4+ Series hardware development][an4555] using the LQFP-100 package.

![Schematic](picture.jpeg)

## Licencing
This PCB desiogn is covered by v1.2 of the [CERN Open Hardware Licence][cern-ohl].
In short you are free to have copies of these PCBs fabricated but if you distribute them (especially for profit) you must provide a link back to the [distribution repository][bitbucket].
In addition by distributing any fabricated PCBs you accept any and all legal responsibilities related to product safety and fitness for purpose.


## Distribution

The main distribution point for this design is [https://bitbucket.org/remyhorton/pcb/src/master/stm32l4r5/][bitbucket].


## Contact

`remy.horton` `(at)` `gmail.com`


[cern-ohl]: https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-1.2
[bitbucket]: https://bitbucket.org/remyhorton/pcb/src/master/stm32l4r5/

[an4555]: https://www.st.com/resource/en/application_note/an4555-getting-started-with-stm32l4-series-and-stm32l4-series-hardware-development-stmicroelectronics.pdf