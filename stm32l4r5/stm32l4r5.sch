EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "STM32L4R5 Breakout"
Date "15 November 2020"
Rev "Rev.2"
Comp "Remy Horton"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_ST_STM32L4+:STM32L4R5VITx U1
U 1 1 5FB09904
P 3400 2100
F 0 "U1" V 2650 4050 50  0000 R CNN
F 1 "STM32L4R5VITx" V 2650 3850 50  0000 R CNN
F 2 "Package_QFP:LQFP-100_14x14mm_P0.5mm" H 2700 -500 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00366448.pdf" H 3400 2100 50  0001 C CNN
	1    3400 2100
	0    1    1    0   
$EndComp
Wire Wire Line
	6100 1900 6100 2000
Wire Wire Line
	6100 2000 6100 2100
Connection ~ 6100 2000
Wire Wire Line
	6100 2100 6100 2200
Connection ~ 6100 2100
Wire Wire Line
	6100 2200 6100 2300
Connection ~ 6100 2200
$Comp
L power:GND #PWR01
U 1 1 5FB10066
P 600 2550
F 0 "#PWR01" H 600 2300 50  0001 C CNN
F 1 "GND" H 605 2377 50  0000 C CNN
F 2 "" H 600 2550 50  0001 C CNN
F 3 "" H 600 2550 50  0001 C CNN
	1    600  2550
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR014
U 1 1 5FB16DEE
P 9400 800
F 0 "#PWR014" H 9400 650 50  0001 C CNN
F 1 "+3V3" H 9415 973 50  0000 C CNN
F 2 "" H 9400 800 50  0001 C CNN
F 3 "" H 9400 800 50  0001 C CNN
	1    9400 800 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR018
U 1 1 5FB16E02
P 10300 1100
F 0 "#PWR018" H 10300 850 50  0001 C CNN
F 1 "GND" H 10305 927 50  0000 C CNN
F 2 "" H 10300 1100 50  0001 C CNN
F 3 "" H 10300 1100 50  0001 C CNN
	1    10300 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	600  1900 600  2000
Wire Wire Line
	600  2000 600  2100
Connection ~ 600  2000
Wire Wire Line
	600  2100 600  2200
Connection ~ 600  2100
Wire Wire Line
	600  2200 600  2300
Connection ~ 600  2200
Wire Wire Line
	600  2300 600  2400
Connection ~ 600  2300
Wire Wire Line
	600  2550 600  2400
Connection ~ 600  2400
$Comp
L power:+3V3 #PWR09
U 1 1 5FB22B33
P 6100 1800
F 0 "#PWR09" H 6100 1650 50  0001 C CNN
F 1 "+3V3" H 6115 1973 50  0000 C CNN
F 2 "" H 6100 1800 50  0001 C CNN
F 3 "" H 6100 1800 50  0001 C CNN
	1    6100 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 2100 6100 2100
$Comp
L power:GND #PWR06
U 1 1 5FB2419C
P 5300 1050
F 0 "#PWR06" H 5300 800 50  0001 C CNN
F 1 "GND" H 5305 877 50  0000 C CNN
F 2 "" H 5300 1050 50  0001 C CNN
F 3 "" H 5300 1050 50  0001 C CNN
	1    5300 1050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5FB24F4F
P 5300 800
F 0 "C1" H 5400 700 50  0000 R CNN
F 1 "100nF" H 5550 900 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5338 650 50  0001 C CNN
F 3 "~" H 5300 800 50  0001 C CNN
	1    5300 800 
	-1   0    0    1   
$EndComp
$Comp
L Device:C C2
U 1 1 5FB25BB9
P 5500 800
F 0 "C2" H 5600 700 50  0000 R CNN
F 1 "1uF" H 5650 900 50  0000 R CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-2012-15_AVX-P_Pad1.30x1.05mm_HandSolder" H 5538 650 50  0001 C CNN
F 3 "~" H 5500 800 50  0001 C CNN
	1    5500 800 
	-1   0    0    1   
$EndComp
Wire Wire Line
	5300 650  5500 650 
Wire Wire Line
	5500 650  5600 650 
Wire Wire Line
	5600 650  5600 1200
Connection ~ 5500 650 
Wire Wire Line
	5300 950  5500 950 
Wire Wire Line
	5300 1050 5300 950 
Connection ~ 5300 950 
Wire Wire Line
	5500 950  5500 1200
Connection ~ 5500 950 
$Comp
L Device:C C7
U 1 1 5FB2C6CE
P 6800 2650
F 0 "C7" H 6800 2750 50  0000 L CNN
F 1 "10nF" H 6800 2550 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6838 2500 50  0001 C CNN
F 3 "~" H 6800 2650 50  0001 C CNN
	1    6800 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5FB2D769
P 6600 2650
F 0 "C6" H 6600 2750 50  0000 L CNN
F 1 "1uF" H 6600 2550 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-2012-15_AVX-P_Pad1.30x1.05mm_HandSolder" H 6638 2500 50  0001 C CNN
F 3 "~" H 6600 2650 50  0001 C CNN
	1    6600 2650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5FB2ED87
P 6450 2850
F 0 "#PWR013" H 6450 2600 50  0001 C CNN
F 1 "GND" H 6455 2677 50  0000 C CNN
F 2 "" H 6450 2850 50  0001 C CNN
F 3 "" H 6450 2850 50  0001 C CNN
	1    6450 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:Ferrite_Bead FB1
U 1 1 5FB31972
P 6700 2350
F 0 "FB1" H 6837 2396 50  0000 L CNN
F 1 "Ferrite_Bead" H 6837 2305 50  0000 L CNN
F 2 "stm32l4r5:Ferrite_0805_HandSolder" V 6630 2350 50  0001 C CNN
F 3 "~" H 6700 2350 50  0001 C CNN
	1    6700 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 2500 6700 2500
Wire Wire Line
	6600 2500 6700 2500
Connection ~ 6700 2500
$Comp
L Device:C C3
U 1 1 5FB35B2F
P 6100 2650
F 0 "C3" H 6100 2750 50  0000 L CNN
F 1 "1uF" H 6100 2550 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-2012-15_AVX-P_Pad1.30x1.05mm_HandSolder" H 6138 2500 50  0001 C CNN
F 3 "~" H 6100 2650 50  0001 C CNN
	1    6100 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5FB35EEF
P 6300 2650
F 0 "C5" H 6300 2750 50  0000 L CNN
F 1 "100nF" H 6300 2550 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6338 2500 50  0001 C CNN
F 3 "~" H 6300 2650 50  0001 C CNN
	1    6300 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 2500 6250 2500
Connection ~ 6100 2500
Wire Wire Line
	6250 2100 6250 2200
Connection ~ 6250 2100
Connection ~ 6250 2500
Wire Wire Line
	6250 2500 6100 2500
Wire Wire Line
	6600 2800 6700 2800
Wire Wire Line
	6300 2800 6200 2800
Wire Wire Line
	6200 2800 6200 2850
Wire Wire Line
	6200 2850 6450 2850
Wire Wire Line
	6700 2850 6700 2800
Connection ~ 6200 2800
Wire Wire Line
	6200 2800 6100 2800
Connection ~ 6700 2800
Wire Wire Line
	6700 2800 6800 2800
Connection ~ 6450 2850
Wire Wire Line
	6450 2850 6700 2850
Wire Wire Line
	6600 2400 6600 2500
Connection ~ 6600 2500
Wire Wire Line
	6250 2200 6700 2200
Connection ~ 6250 2200
Wire Wire Line
	6250 2200 6250 2500
$Comp
L power:+3V3 #PWR011
U 1 1 5FB3E6B0
P 6250 2100
F 0 "#PWR011" H 6250 1950 50  0001 C CNN
F 1 "+3V3" H 6265 2273 50  0000 C CNN
F 2 "" H 6250 2100 50  0001 C CNN
F 3 "" H 6250 2100 50  0001 C CNN
	1    6250 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C12
U 1 1 5FB41057
P 10600 950
F 0 "C12" H 10600 850 50  0000 R CNN
F 1 "100nF" H 10600 1050 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 10638 800 50  0001 C CNN
F 3 "~" H 10600 950 50  0001 C CNN
	1    10600 950 
	-1   0    0    1   
$EndComp
$Comp
L Device:C C11
U 1 1 5FB4120B
P 10300 950
F 0 "C11" H 10300 850 50  0000 R CNN
F 1 "100nF" H 10300 1050 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 10338 800 50  0001 C CNN
F 3 "~" H 10300 950 50  0001 C CNN
	1    10300 950 
	-1   0    0    1   
$EndComp
$Comp
L Device:C C13
U 1 1 5FB415E8
P 10900 950
F 0 "C13" H 10900 850 50  0000 R CNN
F 1 "100nF" H 10900 1050 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 10938 800 50  0001 C CNN
F 3 "~" H 10900 950 50  0001 C CNN
	1    10900 950 
	-1   0    0    1   
$EndComp
$Comp
L Device:C C10
U 1 1 5FB41765
P 10000 950
F 0 "C10" H 10000 850 50  0000 R CNN
F 1 "100nF" H 10000 1050 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 10038 800 50  0001 C CNN
F 3 "~" H 10000 950 50  0001 C CNN
	1    10000 950 
	-1   0    0    1   
$EndComp
$Comp
L Device:C C9
U 1 1 5FB4199B
P 9700 950
F 0 "C9" H 9700 850 50  0000 R CNN
F 1 "100nF" H 9700 1050 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9738 800 50  0001 C CNN
F 3 "~" H 9700 950 50  0001 C CNN
	1    9700 950 
	-1   0    0    1   
$EndComp
$Comp
L Device:C C8
U 1 1 5FB41DCB
P 9400 950
F 0 "C8" H 9400 850 50  0000 R CNN
F 1 "4.7uF" H 9400 1050 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9438 800 50  0001 C CNN
F 3 "~" H 9400 950 50  0001 C CNN
	1    9400 950 
	-1   0    0    1   
$EndComp
$Comp
L power:+3V3 #PWR017
U 1 1 5FB42F78
P 10300 800
F 0 "#PWR017" H 10300 650 50  0001 C CNN
F 1 "+3V3" H 10315 973 50  0000 C CNN
F 2 "" H 10300 800 50  0001 C CNN
F 3 "" H 10300 800 50  0001 C CNN
	1    10300 800 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5FB4390A
P 9400 1100
F 0 "#PWR015" H 9400 850 50  0001 C CNN
F 1 "GND" H 9405 927 50  0000 C CNN
F 2 "" H 9400 1100 50  0001 C CNN
F 3 "" H 9400 1100 50  0001 C CNN
	1    9400 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	10900 800  10600 800 
Connection ~ 10300 800 
Connection ~ 10600 800 
Wire Wire Line
	10600 800  10300 800 
Wire Wire Line
	9700 800  10000 800 
Connection ~ 10000 800 
Wire Wire Line
	10000 800  10300 800 
Wire Wire Line
	9700 1100 10000 1100
Connection ~ 10000 1100
Wire Wire Line
	10000 1100 10300 1100
Connection ~ 10300 1100
Wire Wire Line
	10300 1100 10600 1100
Connection ~ 10600 1100
Wire Wire Line
	10600 1100 10900 1100
$Comp
L Device:R R1
U 1 1 5FB468F6
P 4300 1050
F 0 "R1" H 4450 1000 50  0000 R CNN
F 1 "10k" H 4500 1100 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4230 1050 50  0001 C CNN
F 3 "~" H 4300 1050 50  0001 C CNN
	1    4300 1050
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5FB48A01
P 4050 750
F 0 "#PWR04" H 4050 500 50  0001 C CNN
F 1 "GND" H 4055 577 50  0000 C CNN
F 2 "" H 4050 750 50  0001 C CNN
F 3 "" H 4050 750 50  0001 C CNN
	1    4050 750 
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR05
U 1 1 5FB4917C
P 4550 750
F 0 "#PWR05" H 4550 600 50  0001 C CNN
F 1 "+3V3" H 4565 923 50  0000 C CNN
F 2 "" H 4550 750 50  0001 C CNN
F 3 "" H 4550 750 50  0001 C CNN
	1    4550 750 
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 5FB4C26B
P 6200 1050
F 0 "SW1" H 6200 1335 50  0000 C CNN
F 1 "SW_Push" H 6200 1244 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 6200 1250 50  0001 C CNN
F 3 "~" H 6200 1250 50  0001 C CNN
	1    6200 1050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5FB4CCF3
P 6200 1200
F 0 "C4" H 6200 1100 50  0000 R CNN
F 1 "100nF" H 6200 1300 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6238 1050 50  0001 C CNN
F 3 "~" H 6200 1200 50  0001 C CNN
	1    6200 1200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6000 1050 6000 1200
Wire Wire Line
	6000 1200 6050 1200
Wire Wire Line
	6350 1200 6400 1200
Wire Wire Line
	6400 1200 6400 1050
Wire Wire Line
	6000 1050 5800 1050
Wire Wire Line
	5800 1050 5800 1200
Connection ~ 6000 1050
$Comp
L power:GND #PWR012
U 1 1 5FB523ED
P 6400 1200
F 0 "#PWR012" H 6400 950 50  0001 C CNN
F 1 "GND" H 6405 1027 50  0000 C CNN
F 2 "" H 6400 1200 50  0001 C CNN
F 3 "" H 6400 1200 50  0001 C CNN
	1    6400 1200
	1    0    0    -1  
$EndComp
Connection ~ 6400 1200
$Comp
L Connector:Conn_ARM_JTAG_SWD_20 J7
U 1 1 5FB52D20
P 10000 5500
F 0 "J7" H 9471 5500 50  0000 R CNN
F 1 "Conn_ARM_JTAG_SWD_20" H 9471 5455 50  0001 R CNN
F 2 "Connector_IDC:IDC-Header_2x10_P2.54mm_Vertical" H 10450 4450 50  0001 L TNN
F 3 "http://infocenter.arm.com/help/topic/com.arm.doc.dui0499b/DUI0499B_system_design_reference.pdf" V 9650 4250 50  0001 C CNN
	1    10000 5500
	1    0    0    -1  
$EndComp
Text GLabel 10600 5000 2    50   Input ~ 0
nTRST
Text GLabel 10600 5700 2    50   Input ~ 0
TDI
Text GLabel 10600 5500 2    50   Input ~ 0
TMS
Text GLabel 10600 5400 2    50   Input ~ 0
TCK
Text GLabel 10600 5600 2    50   Input ~ 0
TDO
$Comp
L Device:R R3
U 1 1 5FB595AB
P 10900 5900
F 0 "R3" V 11107 5900 50  0000 C CNN
F 1 "10k" V 11016 5900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 10830 5900 50  0001 C CNN
F 3 "~" H 10900 5900 50  0001 C CNN
	1    10900 5900
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R4
U 1 1 5FB59C33
P 10900 6000
F 0 "R4" V 10700 6000 50  0000 C CNN
F 1 "10k" V 10800 6000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 10830 6000 50  0001 C CNN
F 3 "~" H 10900 6000 50  0001 C CNN
	1    10900 6000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2
U 1 1 5FB5BF0C
P 10900 5300
F 0 "R2" V 10700 5300 50  0000 C CNN
F 1 "10k" V 10800 5300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 10830 5300 50  0001 C CNN
F 3 "~" H 10900 5300 50  0001 C CNN
	1    10900 5300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10750 5300 10600 5300
Wire Wire Line
	10750 5900 10600 5900
Wire Wire Line
	10750 6000 10600 6000
Wire Wire Line
	11050 5300 11050 5900
Wire Wire Line
	11050 5900 11050 6000
Connection ~ 11050 5900
Wire Wire Line
	9900 6300 11050 6300
Wire Wire Line
	11050 6300 11050 6000
Connection ~ 11050 6000
$Comp
L power:GND #PWR019
U 1 1 5FB6325B
P 11050 6300
F 0 "#PWR019" H 11050 6050 50  0001 C CNN
F 1 "GND" H 11055 6127 50  0000 C CNN
F 2 "" H 11050 6300 50  0001 C CNN
F 3 "" H 11050 6300 50  0001 C CNN
	1    11050 6300
	1    0    0    -1  
$EndComp
Connection ~ 11050 6300
$Comp
L power:+3V3 #PWR016
U 1 1 5FB63A92
P 9950 4700
F 0 "#PWR016" H 9950 4550 50  0001 C CNN
F 1 "+3V3" H 9965 4873 50  0000 C CNN
F 2 "" H 9950 4700 50  0001 C CNN
F 3 "" H 9950 4700 50  0001 C CNN
	1    9950 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 4700 9950 4700
Wire Wire Line
	9900 4700 9950 4700
Connection ~ 9950 4700
$Comp
L Jumper:Jumper_3_Bridged12 JP1
U 1 1 5FB7639A
P 4300 750
F 0 "JP1" H 4300 863 50  0000 C CNN
F 1 "Jumper_3_Bridged12" H 4300 863 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4300 750 50  0001 C CNN
F 3 "~" H 4300 750 50  0001 C CNN
	1    4300 750 
	1    0    0    -1  
$EndComp
NoConn ~ 4400 1200
Text GLabel 1600 1200 1    50   Input ~ 0
Tx
Text GLabel 1500 1200 1    50   Input ~ 0
Rx
Text GLabel 1200 1200 1    50   Input ~ 0
SCL
Text GLabel 1100 1200 1    50   Input ~ 0
SDA
Text GLabel 5800 3000 3    50   Input ~ 0
PA0
Text GLabel 4400 3000 3    50   Input ~ 0
TCK
Text GLabel 4500 3000 3    50   Input ~ 0
TMS
Text GLabel 3800 3000 3    50   Input ~ 0
TDO
Text GLabel 4300 3000 3    50   Input ~ 0
TDI
Text GLabel 10600 5100 2    50   Input ~ 0
nReset
Text GLabel 3700 3000 3    50   Input ~ 0
nTRST
Text GLabel 5800 1050 1    50   Input ~ 0
nReset
Text GLabel 5700 3000 3    50   Input ~ 0
PA1
Text GLabel 5600 3000 3    50   Input ~ 0
PA2
Text GLabel 5500 3000 3    50   Input ~ 0
PA3
Text GLabel 5400 3000 3    50   Input ~ 0
PA4
Text GLabel 5300 3000 3    50   Input ~ 0
PA5
Text GLabel 5200 3000 3    50   Input ~ 0
PA6
Text GLabel 5100 3000 3    50   Input ~ 0
PA7
Text GLabel 2400 3000 3    50   Input ~ 0
PC0
Text GLabel 2300 3000 3    50   Input ~ 0
PC1
Text GLabel 2200 3000 3    50   Input ~ 0
PC2
Text GLabel 2100 3000 3    50   Input ~ 0
PC3
Text GLabel 2000 3000 3    50   Input ~ 0
PC4
Text GLabel 1900 3000 3    50   Input ~ 0
PC5
Text GLabel 1800 3000 3    50   Input ~ 0
PC6
Text GLabel 1700 3000 3    50   Input ~ 0
PC7
Text GLabel 1600 3000 3    50   Input ~ 0
PC8
Text GLabel 1500 3000 3    50   Input ~ 0
PC9
Text GLabel 1400 3000 3    50   Input ~ 0
PC10
Text GLabel 1300 3000 3    50   Input ~ 0
PC11
Text GLabel 1200 3000 3    50   Input ~ 0
PC12
Text GLabel 1100 3000 3    50   Input ~ 0
PC13
Text GLabel 1000 3000 3    50   Input ~ 0
PC14
Text GLabel 900  3000 3    50   Input ~ 0
PC15
NoConn ~ 4100 3000
NoConn ~ 4000 3000
NoConn ~ 3900 3000
NoConn ~ 3600 3000
NoConn ~ 3500 3000
NoConn ~ 3400 3000
NoConn ~ 3300 3000
NoConn ~ 3200 3000
NoConn ~ 3100 3000
NoConn ~ 3000 3000
NoConn ~ 900  1200
NoConn ~ 1000 1200
NoConn ~ 1300 1200
NoConn ~ 1400 1200
NoConn ~ 1700 1200
NoConn ~ 1800 1200
NoConn ~ 1900 1200
NoConn ~ 2000 1200
NoConn ~ 2100 1200
NoConn ~ 2200 1200
NoConn ~ 2300 1200
NoConn ~ 2400 1200
NoConn ~ 2600 1200
NoConn ~ 2700 1200
NoConn ~ 2800 1200
NoConn ~ 2900 1200
NoConn ~ 3000 1200
NoConn ~ 3100 1200
NoConn ~ 3200 1200
NoConn ~ 3300 1200
NoConn ~ 3400 1200
NoConn ~ 3500 1200
NoConn ~ 3600 1200
NoConn ~ 3700 1200
NoConn ~ 3800 1200
NoConn ~ 3900 1200
NoConn ~ 4000 1200
NoConn ~ 4100 1200
Text GLabel 4600 3000 3    50   Input ~ 0
USB_DP
Text GLabel 4700 3000 3    50   Input ~ 0
USB_DM
Text GLabel 4800 3000 3    50   Input ~ 0
USB_ID
$Comp
L Connector:USB_B_Mini J6
U 1 1 5FBA84FA
P 10300 1900
F 0 "J6" H 10150 2250 50  0000 C CNN
F 1 "USB_B_Mini" H 10357 2276 50  0001 C CNN
F 2 "Connector_USB:USB_Mini-B_Lumberg_2486_01_Horizontal" H 10450 1850 50  0001 C CNN
F 3 "~" H 10450 1850 50  0001 C CNN
	1    10300 1900
	1    0    0    -1  
$EndComp
Text GLabel 10600 1900 2    50   Input ~ 0
USB_DP
Text GLabel 10600 2000 2    50   Input ~ 0
USB_DM
Text GLabel 10600 2100 2    50   Input ~ 0
USB_ID
Text GLabel 4900 3000 3    50   Input ~ 0
USB_VBUS
Text GLabel 10600 1700 2    50   Input ~ 0
USB_VBUS
$Comp
L power:GND #PWR010
U 1 1 5FBA95DD
P 10300 2300
F 0 "#PWR010" H 10300 2050 50  0001 C CNN
F 1 "GND" H 10305 2127 50  0000 C CNN
F 2 "" H 10300 2300 50  0001 C CNN
F 3 "" H 10300 2300 50  0001 C CNN
	1    10300 2300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x16 J1
U 1 1 5FBADC1D
P 3000 5850
F 0 "J1" V 3217 5796 50  0000 C CNN
F 1 "Conn_01x16" V 3126 5796 50  0000 C CNN
F 2 "stm32l4r5:ConnectorMicromatch-16" H 3000 5850 50  0001 C CNN
F 3 "~" H 3000 5850 50  0001 C CNN
	1    3000 5850
	0    -1   -1   0   
$EndComp
Text GLabel 2300 6050 3    50   Input ~ 0
PC0
Text GLabel 2400 6050 3    50   Input ~ 0
PC1
Text GLabel 2500 6050 3    50   Input ~ 0
PC2
Text GLabel 2600 6050 3    50   Input ~ 0
PC3
Text GLabel 2700 6050 3    50   Input ~ 0
PC4
Text GLabel 2800 6050 3    50   Input ~ 0
PC5
Text GLabel 2900 6050 3    50   Input ~ 0
PC6
Text GLabel 3000 6050 3    50   Input ~ 0
PC7
Text GLabel 3100 6050 3    50   Input ~ 0
PC8
Text GLabel 3800 6050 3    50   Input ~ 0
PC15
Text GLabel 3700 6050 3    50   Input ~ 0
PC14
Text GLabel 3600 6050 3    50   Input ~ 0
PC13
Text GLabel 3500 6050 3    50   Input ~ 0
PC12
Text GLabel 3400 6050 3    50   Input ~ 0
PC11
Text GLabel 3300 6050 3    50   Input ~ 0
PC10
Text GLabel 3200 6050 3    50   Input ~ 0
PC9
$Comp
L Connector_Generic:Conn_01x08 J4
U 1 1 5FBB0C6C
P 5250 6050
F 0 "J4" V 5214 5562 50  0000 R CNN
F 1 "Conn_01x08" V 5123 5562 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 5250 6050 50  0001 C CNN
F 3 "~" H 5250 6050 50  0001 C CNN
	1    5250 6050
	0    -1   -1   0   
$EndComp
Text GLabel 4950 6250 3    50   Input ~ 0
PA0
Text GLabel 5050 6250 3    50   Input ~ 0
PA1
Text GLabel 5150 6250 3    50   Input ~ 0
PA2
Text GLabel 5250 6250 3    50   Input ~ 0
PA3
Text GLabel 5350 6250 3    50   Input ~ 0
PA4
Text GLabel 5450 6250 3    50   Input ~ 0
PA5
Text GLabel 5550 6250 3    50   Input ~ 0
PA6
Text GLabel 5650 6250 3    50   Input ~ 0
PA7
$Comp
L power:GND #PWR07
U 1 1 5FBB2D70
P 9750 2000
F 0 "#PWR07" H 9750 1750 50  0001 C CNN
F 1 "GND" H 9755 1827 50  0000 C CNN
F 2 "" H 9750 2000 50  0001 C CNN
F 3 "" H 9750 2000 50  0001 C CNN
	1    9750 2000
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR08
U 1 1 5FBB2F16
P 10050 1650
F 0 "#PWR08" H 10050 1500 50  0001 C CNN
F 1 "+3V3" H 10065 1823 50  0000 C CNN
F 2 "" H 10050 1650 50  0001 C CNN
F 3 "" H 10050 1650 50  0001 C CNN
	1    10050 1650
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5FBB432F
P 9400 1700
F 0 "#FLG01" H 9400 1775 50  0001 C CNN
F 1 "PWR_FLAG" H 9400 1873 50  0000 C CNN
F 2 "" H 9400 1700 50  0001 C CNN
F 3 "~" H 9400 1700 50  0001 C CNN
	1    9400 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 2400 6450 2400
NoConn ~ 10200 2300
$Comp
L Connector_Generic:Conn_01x03 J2
U 1 1 5FBB6C2D
P 8250 1700
F 0 "J2" V 8350 1750 50  0000 L CNN
F 1 "Conn_01x03" V 8213 1880 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 8250 1700 50  0001 C CNN
F 3 "~" H 8250 1700 50  0001 C CNN
	1    8250 1700
	0    -1   -1   0   
$EndComp
Text GLabel 8250 1900 3    50   Input ~ 0
SDA
Text GLabel 8350 1900 3    50   Input ~ 0
SCL
$Comp
L Connector_Generic:Conn_01x03 J3
U 1 1 5FBBA5C4
P 8600 1700
F 0 "J3" V 8700 1750 50  0000 L CNN
F 1 "Conn_01x03" V 8563 1880 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 8600 1700 50  0001 C CNN
F 3 "~" H 8600 1700 50  0001 C CNN
	1    8600 1700
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5FBBA8C5
P 8500 2050
F 0 "#PWR03" H 8500 1800 50  0001 C CNN
F 1 "GND" H 8505 1877 50  0000 C CNN
F 2 "" H 8500 2050 50  0001 C CNN
F 3 "" H 8500 2050 50  0001 C CNN
	1    8500 2050
	1    0    0    -1  
$EndComp
Text GLabel 8600 1900 3    50   Input ~ 0
Rx
Text GLabel 8700 1900 3    50   Input ~ 0
Tx
$Comp
L power:GND #PWR02
U 1 1 5FBBB297
P 8150 2050
F 0 "#PWR02" H 8150 1800 50  0001 C CNN
F 1 "GND" H 8155 1877 50  0000 C CNN
F 2 "" H 8150 2050 50  0001 C CNN
F 3 "" H 8150 2050 50  0001 C CNN
	1    8150 2050
	1    0    0    -1  
$EndComp
NoConn ~ 5000 3000
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5FBBD663
P 6450 2400
F 0 "#FLG02" H 6450 2475 50  0001 C CNN
F 1 "PWR_FLAG" H 6450 2573 50  0001 C CNN
F 2 "" H 6450 2400 50  0001 C CNN
F 3 "~" H 6450 2400 50  0001 C CNN
	1    6450 2400
	1    0    0    -1  
$EndComp
Connection ~ 6450 2400
Wire Wire Line
	6450 2400 6600 2400
Text Notes 5000 3450 0    50   ~ 0
There is warning\nabout PA9 being\nconnected to VBUS
$Comp
L Regulator_Linear:uA7805 U2
U 1 1 5FBBE8E4
P 9750 1700
F 0 "U2" H 9750 1850 50  0000 C CNN
F 1 "uA7805" H 9750 1851 50  0001 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 9775 1550 50  0001 L CIN
F 3 "http://www.ti.com/lit/ds/symlink/ua78.pdf" H 9750 1650 50  0001 C CNN
	1    9750 1700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Barrel_Jack J5
U 1 1 5FBBF824
P 9000 1800
F 0 "J5" H 9057 2033 50  0000 C CNN
F 1 "Barrel_Jack" H 9057 2034 50  0001 C CNN
F 2 "Connector_BarrelJack:BarrelJack_Horizontal" H 9050 1760 50  0001 C CNN
F 3 "~" H 9050 1760 50  0001 C CNN
	1    9000 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 2000 9300 1900
Wire Wire Line
	9300 2000 9750 2000
Connection ~ 9750 2000
Wire Wire Line
	9450 1700 9400 1700
Connection ~ 9400 1700
Wire Wire Line
	9400 1700 9300 1700
Text GLabel 4500 1200 1    50   Input ~ 0
Clock
$Comp
L Oscillator:SG-8002CA X1
U 1 1 5FB1C821
P 10200 3450
F 0 "X1" H 10300 3700 50  0000 L CNN
F 1 "SG-8002CA" H 10544 3405 50  0001 L CNN
F 2 "Oscillator:Oscillator_SMD_SeikoEpson_SG8002CA-4Pin_7.0x5.0mm" H 10900 3100 50  0001 C CNN
F 3 "https://support.epson.biz/td/api/doc_check.php?mode=dl&lang=en&Parts=SG-8002DC" H 10100 3450 50  0001 C CNN
	1    10200 3450
	1    0    0    -1  
$EndComp
Text GLabel 10500 3450 2    50   Input ~ 0
Clock
$Comp
L power:GND #PWR0101
U 1 1 5FB22273
P 10200 3750
F 0 "#PWR0101" H 10200 3500 50  0001 C CNN
F 1 "GND" H 10205 3577 50  0000 C CNN
F 2 "" H 10200 3750 50  0001 C CNN
F 3 "" H 10200 3750 50  0001 C CNN
	1    10200 3750
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0102
U 1 1 5FB22670
P 10200 3150
F 0 "#PWR0102" H 10200 3000 50  0001 C CNN
F 1 "+3V3" H 10215 3323 50  0000 C CNN
F 2 "" H 10200 3150 50  0001 C CNN
F 3 "" H 10200 3150 50  0001 C CNN
	1    10200 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9900 3450 9900 3150
Wire Wire Line
	9900 3150 10200 3150
Connection ~ 10200 3150
Wire Wire Line
	8500 1900 8500 2050
Wire Wire Line
	8150 1900 8150 2050
Wire Wire Line
	10050 1650 10050 1700
Text GLabel 2900 3000 3    50   Input ~ 0
LED1
Text GLabel 2800 3000 3    50   Input ~ 0
LED2
Text GLabel 2700 3000 3    50   Input ~ 0
LED3
Text GLabel 2600 3000 3    50   Input ~ 0
LED4
Text GLabel 9450 3200 1    50   Input ~ 0
LED1
Text GLabel 9100 3200 1    50   Input ~ 0
LED2
Text GLabel 8700 3200 1    50   Input ~ 0
LED3
Text GLabel 8350 3200 1    50   Input ~ 0
LED4
$Comp
L Device:R R7
U 1 1 5FBECFEE
P 9100 3650
F 0 "R7" H 9170 3696 50  0000 L CNN
F 1 "1k" H 9170 3605 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9030 3650 50  0001 C CNN
F 3 "~" H 9100 3650 50  0001 C CNN
	1    9100 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 5FBE9290
P 9100 3350
F 0 "D3" V 9139 3232 50  0000 R CNN
F 1 "LED" V 9048 3232 50  0000 R CNN
F 2 "LED_THT:LED_Rectangular_W5.0mm_H2.0mm" H 9100 3350 50  0001 C CNN
F 3 "~" H 9100 3350 50  0001 C CNN
	1    9100 3350
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R8
U 1 1 5FBEF33C
P 9450 3650
F 0 "R8" H 9520 3696 50  0000 L CNN
F 1 "1k" H 9520 3605 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9380 3650 50  0001 C CNN
F 3 "~" H 9450 3650 50  0001 C CNN
	1    9450 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D4
U 1 1 5FBEF346
P 9450 3350
F 0 "D4" V 9489 3232 50  0000 R CNN
F 1 "LED" V 9398 3232 50  0000 R CNN
F 2 "LED_THT:LED_Rectangular_W5.0mm_H2.0mm" H 9450 3350 50  0001 C CNN
F 3 "~" H 9450 3350 50  0001 C CNN
	1    9450 3350
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R5
U 1 1 5FBF1ECB
P 8350 3650
F 0 "R5" H 8420 3696 50  0000 L CNN
F 1 "1k" H 8420 3605 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8280 3650 50  0001 C CNN
F 3 "~" H 8350 3650 50  0001 C CNN
	1    8350 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 5FBF1ED5
P 8350 3350
F 0 "D1" V 8389 3232 50  0000 R CNN
F 1 "LED" V 8298 3232 50  0000 R CNN
F 2 "LED_THT:LED_Rectangular_W5.0mm_H2.0mm" H 8350 3350 50  0001 C CNN
F 3 "~" H 8350 3350 50  0001 C CNN
	1    8350 3350
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R6
U 1 1 5FBF1EDF
P 8700 3650
F 0 "R6" H 8770 3696 50  0000 L CNN
F 1 "1k" H 8770 3605 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8630 3650 50  0001 C CNN
F 3 "~" H 8700 3650 50  0001 C CNN
	1    8700 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D2
U 1 1 5FBF1EE9
P 8700 3350
F 0 "D2" V 8739 3232 50  0000 R CNN
F 1 "LED" V 8648 3232 50  0000 R CNN
F 2 "LED_THT:LED_Rectangular_W5.0mm_H2.0mm" H 8700 3350 50  0001 C CNN
F 3 "~" H 8700 3350 50  0001 C CNN
	1    8700 3350
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5FBF694F
P 8900 3800
F 0 "#PWR0103" H 8900 3550 50  0001 C CNN
F 1 "GND" H 8905 3627 50  0000 C CNN
F 2 "" H 8900 3800 50  0001 C CNN
F 3 "" H 8900 3800 50  0001 C CNN
	1    8900 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 3800 8700 3800
Connection ~ 8700 3800
Wire Wire Line
	8700 3800 8900 3800
Connection ~ 8900 3800
Wire Wire Line
	8900 3800 9100 3800
Connection ~ 9100 3800
Wire Wire Line
	9100 3800 9450 3800
$EndSCHEMATC
