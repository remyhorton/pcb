Thermocouple sensor circuit, rev.3
==================================
A circuit for temperature readings from thermocouples.
This revision omits an on-board microcontroller and one of the four ADC inputs is used for the reference temperature, which is intended to make this an all-in daughter-board.
Information on interfacing with this PCB is given [in an article][article] on my website.

## Circuit schematic

![Schematic](schematic.png)


## Components

| Component                | Manufacturer     | Part number
| :----------------------: | :--------------: | :---------:
| Thermocouple             | Labfacility      | `Z3-K-1M`
| Analog-digital converter | Microchip        | `MCP3428`
| Temperature sensor chip  | Texas Instuments | `LM35DM/NOPB`


## Licencing
See the [top-level Readme.md](../Readme.md) for licencing details.


## Contact

`remy.horton` `(at)` `gmail.com`


[article]: http://www.remy.org.uk/elec.php/elec.php?elec=1602198000