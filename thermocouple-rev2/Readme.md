Thermocouple sensor circuit, rev.2
==================================
A circuit for temperature readings from a thermocouple.
This second revision omits an on-board microcontroller as it is intended to be used as a daughter-board.

## Circuit schematic

![Schematic](schematic.png)


## Components

| Component                | Manufacturer     | Part number
| :----------------------: | :--------------: | :---------:
| Thermocouple             | Labfacility      | `Z3-K-1M`
| Analog-digital converter | Microchip        | `MCP3428`
| Temperature sensor chip  | Texas Instuments | `LM35DM/NOPB`


## Licencing
See the [top-level Readme.md](../Readme.md) for licencing details.


## Contact

`remy.horton` `(at)` `gmail.com`
