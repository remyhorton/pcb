Thermocouple sensor circuit
===========================
A circuit for temperature readings from a thermocouple. It has connections for up to four thermocouples and using an on-board chip thermometer chip for the cold junction reference temperature it outputs the temperature readings via TTL-level RS232.
The full background is covered in [an article][remy-article] on my personal website.


## Circuit schematic

![Schematic](schematic.png)


## Components

| Component                | Manufacturer     | Part number
| :----------------------: | :--------------: | :---------:
| Thermocouple             | Labfacility      | `Z3-K-1M`
| Microcontroller          | Microchip        | `PIC12F1840-I/P`
| Analog-digital converter | Microchip        | `MCP3428`
| Temperature sensor chip  | Texas Instuments | `LM35DM/NOPB`


## Licencing
See the [top-level Readme.md](../Readme.md) for licencing details.


## Contact

`remy.horton` `(at)` `gmail.com`


[remy-article]: https://www.remy.org.uk/elec.php?elec=1596150000
